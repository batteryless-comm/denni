# COMPRESSION: PRUNING and QUANTIZATION of HUMAN ACTIVITY RECOGNITION (HAR) Benchmark

Goal was to investigate the impact of compression i.e. pruning and quantization on the overall test accuracy vs inference memory footprint.
We implemented the neuron pruning as per the following PAPER:
H. Hu et al., “Network trimming: A data-driven neuron pruning approach towards efficient deep architectures,” arXiv preprint arXiv:1607.03250, 2016.

# To replicate the HAR pruning results perform the following
- Note that to execute any colab script go to link: https://colab.research.google.com/ and upload the script. Then within that script upload the required files as per the PREQUISITE mentioned in the script.
- To execute the neuron pruning of HAR run the script HAR_Neuron_Pruning.ipynb in colab_scripts.
- To execute the quantization of fp32 h5 pruned models to tensorflow lite int8 model run the script HAR_h5_to_tflite.ipynb in colab_scripts.  
- To plot the overall test accuracy vs inference memory footprint plots run the HAR_Pruning_Pareto_Plot.ipynb in colab_scripts.


## References
<a id="1">[1]</a> 
A. Ignatov, “Real-time human activity recognition from accelerometer
data using convolutional neural networks,” Applied
Soft Computing, vol. 62, pp. 915–922, 2018.

<a id="2">[2]</a> 
https://github.com/aiff22/HAR

<a id="3">[3]</a> 
https://github.com/lutzroeder/netron

<a id="4">[4]</a> 
https://www.tensorflow.org/lite/performance/post_training_quantization
