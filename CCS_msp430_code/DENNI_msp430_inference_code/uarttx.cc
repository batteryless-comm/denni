
#include <msp430fr5994.h>





void uartTX(char * tx_data)                 // Define a function which accepts a character pointer to an array
{
    unsigned int i=0;
    while(tx_data[i])                        // Increment through array, look for null pointer (0) at end of string
    {
        while ((UCA0STATW & UCBUSY));        // Wait if line TX/RX module is busy with data
        UCA0TXBUF = tx_data[i];              // Send out element i of tx_data array on UART bus
        i++;                                 // Increment variable for array address
    }
}


void uartConfig() {
    // Set P2.0 and P2.1 for USCI_A0 UART operation
    P2SEL0 &= ~(BIT0 | BIT1);
    P2SEL1 |= BIT0 | BIT1;
    UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
    UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
    UCA0BRW = 8;                             // 8000000/16/9600
    UCA0MCTLW |= UCOS16 | UCBRF_5 | 0xF7;
    UCA0CTLW0 &= ~UCSWRST;
}
