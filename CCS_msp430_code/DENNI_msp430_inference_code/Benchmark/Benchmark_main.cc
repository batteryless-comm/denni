/* Copyright 2018 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#include <stdio.h>
#include <stdint.h>
#include <Benchmark/Model/model.h> // model_current
#include <Benchmark/Test_Inputs/test_input.h> // input_current
#include "tensorflow/lite/micro/kernels/micro_ops.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/micro/micro_mutable_op_resolver.h"
#include "tensorflow/lite/micro/testing/micro_test.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include "tensorflow/lite/version.h"
#include "uarttx.h"



#if defined(__MSP430__)

#include <msp430.h>

// The FILL Macro's helps us to easily initialize Static tensor_arena memory
#define FILL_16 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1
#define FILL_64 FILL_16,FILL_16,FILL_16,FILL_16
#define FILL_256 FILL_64,FILL_64,FILL_64,FILL_64
#define FILL_1024 FILL_256,FILL_256,FILL_256,FILL_256
#define FILL_4096 FILL_1024,FILL_1024,FILL_1024,FILL_1024
#define FILL_5120 FILL_4096, FILL_1024
#define FILL_5120 FILL_4096, FILL_1024
#define FILL_10240 FILL_5120, FILL_5120



TF_LITE_MICRO_TESTS_BEGIN
WDTCTL = WDTPW | WDTHOLD;               // Stop WDT

// To run at 16 MHz
P1OUT &= ~BIT0;                         // Clear P1.0 output latch for a defined power-on state
P1DIR |= BIT0;                          // Set P1.0 to output direction

PM5CTL0 &= ~LOCKLPM5;                   // Disable the GPIO power-on default high-impedance mode
FRCTL0 = FRCTLPW | NWAITS_1;
// Clock System Setup
// Setting the frequency to 16MHz
CSCTL0_H = CSKEY_H;                     // Unlock CS registers
CSCTL1 = DCOFSEL_0;                     // Set DCO to 1MHz
// Set SMCLK = MCLK = DCO, ACLK = VLOCLK
CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;
// Per Device Errata set divider to 4 before changing frequency to
// prevent out of spec operation from overshoot transient
CSCTL3 = DIVA__4 | DIVS__4 | DIVM__4;   // Set all corresponding clk sources to divide by 4 for errata
CSCTL1 = DCOFSEL_4 | DCORSEL;           // Set DCO to 16MHz
// Delay by ~10us to let DCO settle. 60 cycles = 20 cycles buffer + (10us / (1/4MHz))
__delay_cycles(60);
CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;   // Set all dividers to 1 for 16MHz operation
CSCTL0_H = 0;                           // Lock CS registers

uartConfig();

                                             // to activate previously configured port settings





#else
TF_LITE_MICRO_TESTS_BEGIN
#endif


    TF_LITE_MICRO_TEST(TestInvoke) {



                tflite::MicroErrorReporter micro_error_reporter;
                tflite::ErrorReporter *error_reporter = &micro_error_reporter;

                // Map the model into a usable data structure. This doesn't involve any
                // copying or parsing, it's a very lightweight operation.

                const tflite::Model *model =
                        ::tflite::GetModel(model_current);
                if (model->version() != TFLITE_SCHEMA_VERSION) {
                    error_reporter->Report(
                            "Model provided is schema version %d not equal "
                            "to supported version %d.\n",
                            model->version(), TFLITE_SCHEMA_VERSION);
                }


                /*
                 Pull in only the operation implementations we need.
                 This relies on a complete list of all the ops needed by this graph.
                 An easier approach is to just use the AllOpsResolver, but this will
                 incur some penalty in code space for op implementations that are not
                 needed by this graph.

                /*Here you must add only the required operators for your model inference graph which you can visualize in netron.
                 * This also allows to analyze code size in ccs using: View->Memory allocation on the project.
                 * msp430fr5994 has two memory segment: ROM (49024 Bytes) and HIFRAM (212991 Bytes).
                 * We use HIFRAM to store code memory(.upper.text), model memory(.upper.rodata) and tensor arena memory(.upper) which can be seen in View->Memory allocation after compilation
                 * This also provides us the memory constrain of 212991 Bytes to fit the text+ tensor-arena+ model memory*/

                static tflite::MicroMutableOpResolver micro_mutable_op_resolver;


//
//                micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_QUANTIZE,
//                                                     tflite::ops::micro::Register_QUANTIZE());
////https://git.ece.iastate.edu/duwe_group/denni_ipccc.git
//                micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_RESHAPE,
//                                                     tflite::ops::micro::Register_RESHAPE());


                micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_CONV_2D,
                                                     tflite::ops::micro::Register_CONV_2D(), 1, 1);

//                micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_DEPTHWISE_CONV_2D,
//                                                     tflite::ops::micro::Register_DEPTHWISE_CONV_2D(), 1, 1);
//
//
//                micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_AVERAGE_POOL_2D,
//                                                     tflite::ops::micro::Register_AVERAGE_POOL_2D(), 1, 1);

//
//                micro_mutable_op_resolver.AddBuiltin(
//                        tflite::BuiltinOperator_FULLY_CONNECTED,
//                        tflite::ops::micro::Register_FULLY_CONNECTED(), 4, 4);
//
//
//                micro_mutable_op_resolver.AddBuiltin(
//                        tflite::BuiltinOperator_SOFTMAX,
//                        tflite::ops::micro::Register_SOFTMAX(), 1, 1);
////
//                micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_DEQUANTIZE,
//                                                     tflite::ops::micro::Register_DEQUANTIZE(), 2, 2);
////
//                micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_PADV2,
//                                                     tflite::ops::micro::Register_PADV2());






                // Tensor arena is initialized as a static region in HIFRAM.upper., for any model.tflite you could use the arena_estimation script located in folder tensorflow_lite_arena_estimation
                const unsigned long tensor_arena_size = (unsigned long)(30720);
                               __attribute__((section(".upper.")))
                               static uint8_t tensor_arena[tensor_arena_size] = {FILL_10240,FILL_10240,FILL_10240};




                // Build an interpreter to run the model with.
                tflite::MicroInterpreter interpreter(model, micro_mutable_op_resolver,
                                                     tensor_arena, tensor_arena_size,
                                                     error_reporter);
                interpreter.AllocateTensors();

                // Get information about the memory area to use for the model's input.
                TfLiteTensor *input = interpreter.input(0);

                 //Make sure the input has the properties we expect. Update this with the input dimensions
                TF_LITE_MICRO_EXPECT_NE(nullptr, input);
                TF_LITE_MICRO_EXPECT_EQ(4, input->dims->size);
                TF_LITE_MICRO_EXPECT_EQ(1, input->dims->data[0]);
                TF_LITE_MICRO_EXPECT_EQ(1, input->dims->data[1]);
                TF_LITE_MICRO_EXPECT_EQ(1, input->dims->data[2]);
                TF_LITE_MICRO_EXPECT_EQ(768, input->dims->data[3]);
                TF_LITE_MICRO_EXPECT_EQ(kTfLiteUInt8, input->type);


                // Load the input data
                const uint8 *input_data = input_current; // was input_baseline
                for (int i = 0; i < (input->bytes); ++i) {
                    input->data.uint8[i] = input_data[i];
                }


                // Run the model inference on this input and make sure it succeeds.
                TfLiteStatus invoke_status = interpreter.Invoke();
                if (invoke_status != kTfLiteOk) {
                    error_reporter->Report("Invoke failed\n");
               // }
                TF_LITE_MICRO_EXPECT_EQ(kTfLiteOk, invoke_status);
                 }


                // Get the output from the model, and make sure it's the expected size and
                // type. Please make sure you provide the correct output dimensions as per the model.tflite
                TfLiteTensor *output = interpreter.output(0);
                TF_LITE_MICRO_EXPECT_EQ(2, output->dims->size);
//                TF_LITE_MICRO_EXPECT_EQ(1, output->dims->data[0]);
//                TF_LITE_MICRO_EXPECT_EQ(8, output->dims->data[1]);
//                TF_LITE_MICRO_EXPECT_EQ(8, output->dims->data[2]);
//                TF_LITE_MICRO_EXPECT_EQ(48, output->dims->data[3]);




                // Code to print the max and top values of classification vector:
//        int max_value=0; // stores the max value
//        int max_index=0; // stores the max index
//        for(int i=0;i<1001;i++){
//            //printf("%d\n", (uint8)(output->data.uint8[i]));
//            if(output->data.uint8[i] > max_value){
//                max_value= output->data.uint8[i];
//                max_index=i;
//            }
//
//        }
//        printf("Predicted Output Class is: %d\n", max_index);
//        printf("Uint8 value after softmax of  the predicted class is: %d\n", max_value);


//            }
//
//        }


TF_LITE_MICRO_TESTS_END
}
