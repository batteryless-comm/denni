# Executing and benchmarking (latency, energy and memory) inference for a tensorflow-lite model on msp430fr5994 devices
 Goal of the script is to provide a sample template as a CCS project which uses our C/C++ custom 16-bit tensorflow-lite runtime to execute inference of any model.tflite 

# Steps to run
- First upload the tflite_model_to_hex_array.ipynb in google colab and upload the model.tflite file to the colab script.
- Execute the tflite_model_to_hex_array.ipynb to get the model.h file. Copy and paste only the hex array content inside DENNI_msp430_inference_code/Benchmark/Model/model.h making sure nothing else (like .upper.rodata pragma and model_current name) in model.h is changed.
- Similarly update the content of test_input.h file inside folder DENNI_msp430_inference_code/Benchmark/Test_Inputs
- Go through and follow the instructions mentioned in the Benchmark_main.cc file.


# Code Memory Benchmarking:
- To see the memory usage: compile and then use  View->Memory allocation
- We use HIFRAM to store code memory(.upper.text), model memory(.upper.rodata) and tensor arena memory(.upper)


# Energy and Latency Benchmarking:
- Actual inference happens in line 193 interpreter.Invoke(). Put a debug point after the line and connect the msp430fr5994 launchpad. Launch code in debug mode and laucn energy trace (make sure to change its setting to "on halt")
- You should be able to see both energy and latency in energy trace plus when the inference stops at the debug point.

# Final output:
- To see the final output, you can view the output->data.f array in the variable window in debug mode.
- You could also use uart functions to send the final output or any layer desired intermediate output back to PC an example is shown in tensorflow\lite\kernels\internal\reference\depthwiseconv_uint8.h
- On the PC side you could use the serial_reader.py script and execute it to before inference on the msp430 by making sure you provide the correct uart port to save the inference_output.txt recieved from the msp430fr5994 device.

