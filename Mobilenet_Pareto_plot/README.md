# Mobilenets Test Accuracy vs Total Inference memory on msp430fr5994 using tensorflow-lite Analysis and Pareto curve plotting 
Goal of the script is to study the variation in overall test accuracy vs total inference memory on msp430fr5994 when using a custom 16-bit tensorflow-lite implementation

# Steps to Run
- To replicate the mobilenet pareto results execute the script Mobilenet_pareto_plot.ipynb
