#ifndef FLATBUFFERS_CPP_OUTPUT_SPLIT_CPP_H
vector<uint8_t> extract_vec(vector<uint8_t>& e,
                            int X, int Y);
void concat_vec(vector<uint8_t>& v1,
                vector<uint8_t>& v2);

void conv_depthwise_conv_output_split(string model_name, double no_of_output_channel_per_node);

void conv_depthwise_conv_output_split(string model_name, double no_of_output_channel_per_node, int layer_index);

double get_N_or_M(string model_name);

#define FLATBUFFERS_CPP_OUTPUT_SPLIT_CPP_H

#endif //FLATBUFFERS_CPP_OUTPUT_SPLIT_CPP_H
