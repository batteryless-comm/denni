//
// Created by mailr on 3/26/2021.
//

#ifndef FLATBUFFERS_CPP_CROSSFUSEGEN_H
// Include the necessary files
#include "flatbuffers/flatbuffers.h"
#include "schema_generated.h"
#include <fstream>   // C++ header file for file access
#include <iostream>  // C++ header file for printing
#include <cstdio>
#include "flatbuffers/idl.h"
#include "flatbuffers/registry.h"
#include "flatbuffers/util.h"
using namespace std;
int max_int(int a, int b);
int min_int(int a, int b);
void Add_PADV2(shared_ptr<tflite::ModelT> model_base,int l, int left_pad, int right_pad, int top_pad, int bottom_pad );
void Add_PADV2_top(shared_ptr<tflite::ModelT> model_base,int l, int left_pad, int right_pad, int top_pad, int bottom_pad );
void  extract_layer_params(shared_ptr<tflite::ModelT> model_base_extract, int *inx, int *iny, int *kx, int *ky, int *S, int *pad, int *outx, int *outy, int l);
void track_cross_fuse(int inx, int iny, int kx, int ky, int S, int pad, int outx, int outy, int outx1, int outy1, int outx2, int outy2, int *inx1, int *iny1, int *inx2, int *iny2,int *top_pad, int *bottom_pad,int *left_pad, int *right_pad, int *Tx, int *Ty );
int crossfusegen_main(int p1, int q1, int p2, int q2);
#define FLATBUFFERS_CPP_CROSSFUSEGEN_H

#endif //FLATBUFFERS_CPP_CROSSFUSEGEN_H
