// Code to split the conv3 layer of the USC dominated by the weights
#include "flatbuffers/flatbuffers.h"
#include "schema_generated.h"
#include <fstream>   // C++ header file for file access
#include <iostream>  // C++ header file for printing
#include <cstdio>
#include "flatbuffers/idl.h"
#include "flatbuffers/registry.h"
#include "flatbuffers/util.h"
#include <cmath>
using namespace std;

// Extract subvector at X, Y
vector<uint8_t> extract_vec(vector<uint8_t>& e,
                            int X, int Y)
{

    // Starting and Ending iterators
    auto start = e.begin() + X;
    auto end =   e.begin() + Y + 1;

    // To store the sliced vector
    vector<uint8_t> extract(Y - X + 1);

    // Copy vector using copy function()
    copy(start, end, extract.begin());

    // Return the final sliced vector
    return extract;
}


// concat vector 2 into vector 1
void concat_vec(vector<uint8_t>& v1,
                vector<uint8_t>& v2){
    v1.insert( v1.end(), v2.begin(), v2.end() );

}

void conv_depthwise_conv_output_split(string model_name, double no_of_output_channel_per_node, int layer_index){ // where M are the number of output channels computed per node

    // Read file into a malloced byte array
    FILE *f = fopen(model_name.c_str(), "rb"); // input the tflite model to split
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buf = (char *)malloc(fsize);
    fread(buf, fsize, 1, f);
    fclose(f);

    auto model = tflite::GetMutableModel(buf); // to get the mutable model from the buffer

    auto resolver = flatbuffers::resolver_function_t( // Get the resolver used to unpack the buffer into another model for modifying it
            [](void **pointer_adr, flatbuffers::hash_value_t hash) {
                (void)pointer_adr;
                (void)hash;
                // Don't actually do anything, leave variable null.
            });

    auto rehasher = flatbuffers::rehasher_function_t( // Get the rehasher used to create a flatbuffer latter
            [](void *pointer) -> flatbuffers::hash_value_t {
                (void)pointer;
                return 0;
            });
    printf("\nExecuting automated weight splitting of the provided layer...\n");
    // Since we are partitioning only a single convolution layer, we will have only a single convolution operation
    // A single convolution operation will have three input tensors (input, weights and biases) and one output tensor (output)
    // Each of these tensors will point to a buffer in the model that will store actual weights/activations corresponding to the tensor
    // First we have to find the indexes of the tensors and buffers they point for the corresponding (input, weights, bias and output)
    // We leverage the property that the operator stores input, weights, biases and output in order.
    // For e.g. model1->subgraphs[0]->operators[0]->inputs[0] will always be input tensors, model1->subgraphs[0]->operators[0]->inputs[1] will always be the weights tensor and model1->subgraphs[0]->operators[0]->inputs[2] will always be the bias tensor

    auto model1 = tflite::UnPackModel(buf, &resolver); // Unpack the buf into another model for modification


    auto tensor_input= model1->subgraphs[0]->operators[0]->inputs[0];
    auto buffer_tensor_input= model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->inputs[0]]->buffer;

    auto tensor_weights= model1->subgraphs[0]->operators[0]->inputs[1];
    auto buffer_tensor_weights= model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->inputs[1]]->buffer;

    auto tensor_bias= model1->subgraphs[0]->operators[0]->inputs[2];
    auto buffer_tensor_bias= model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->inputs[2]]->buffer;

    auto tensor_output= model1->subgraphs[0]->operators[0]->outputs[0];
    auto buffer_tensor_output= model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->outputs[0]]->buffer;

    // Figure out the values of weight dimensions R and S and output channel M and input channel N
    auto M = model1->subgraphs[0]->tensors[tensor_weights]->shape[0];
    auto R = model1->subgraphs[0]->tensors[tensor_weights]->shape[1];
    auto S = model1->subgraphs[0]->tensors[tensor_weights]->shape[2];
    auto N = model1->subgraphs[0]->tensors[tensor_weights]->shape[3];

//    printf("%d\n",M);
//    printf("%d\n",R);
//    printf("%d\n",S);
//    printf("%d\n",N);

    // Here W will contain the total weights in bytes / total number of weights to split among the nodes "Nodes"
    int W = model1->buffers[buffer_tensor_weights]->data.size();  // so the weights would be indexed from 0 to W-1



    // Detect if the layer is Conv2D or Depthwise
    auto opocode_index_current= model1->subgraphs[0]->operators[0]->opcode_index;
    auto code= model1->operator_codes[opocode_index_current]->builtin_code;


    if(code==3 )// layer is Conv2D and has only one operator
    {
        double X= no_of_output_channel_per_node; // no of output channels to compute per node

        if(X==M){ // that means no output splitting is neeeded
            printf("No output splitting needed for model: %s\n",model_name.c_str());
            return;
        }

        // find out the total number of nodes depending upon the value of X
        int total_nodes = ceil((M/X));
        //printf("total nodes: %d\n",total_nodes);

        // except the last node each of the (total_nodes-1) nodes would have R*S*N*X weights which we denote by small w
        auto w=R*S*N*X;

        // so except the last node for each node n where n goes from 0 to (total_nodes-1), we needs to keep the weights from index: [nw,(n+1)*w-1]
        // for the last node we need to keep the weights from [(n+1)*w, W-1]
        // so for ach node n where n goes from 0 to (total_nodes-2), we needs to erase the on required partitions i.e. [0,nw-1] and [(n+1)*w,W-1]
        // for last node where n= (total_nodes-1), we need to erase the partition from [0,(n+1)*w-1]
        for(int n=0;n<total_nodes;n++){
            auto model_modify = tflite::UnPackModel(buf, &resolver); // Unpack the buf into another model for modification
            if(n<(total_nodes-1)){ // nodes except the last node

                model_modify->subgraphs[0]->tensors[tensor_weights]->shape[0]=X;// Modify the output shape of the weights tensor
                model_modify->subgraphs[0]->tensors[tensor_bias]->shape[0]=X;// Modify the output shape of the bias tensor
                model_modify->subgraphs[0]->tensors[tensor_output]->shape[3]=X; // Modify the output shape of the output tensor


                if(n==0){ // if first node
                    // erase the non needed weight
                    model_modify->buffers[buffer_tensor_weights].get()->data.erase(model_modify->buffers[buffer_tensor_weights].get()->data.begin()+w, model_modify->buffers[buffer_tensor_weights].get()->data.end());
                    // erase the non needed biases
                    model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*X, model_modify->buffers[buffer_tensor_bias].get()->data.end());


                }
                else{ // if not the first node
                    // first erase the weights on the right
                    model_modify->buffers[buffer_tensor_weights].get()->data.erase(model_modify->buffers[buffer_tensor_weights].get()->data.begin()+(n+1)*w, model_modify->buffers[buffer_tensor_weights].get()->data.end());
                    // them delete the weights on the left
                    model_modify->buffers[buffer_tensor_weights].get()->data.erase(model_modify->buffers[buffer_tensor_weights].get()->data.begin(), model_modify->buffers[buffer_tensor_weights].get()->data.begin()+n*w);

                    // do similar erase for the biases
                    // first erase the weights on the right
                    model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n+1)*X, model_modify->buffers[buffer_tensor_bias].get()->data.end());
                    // them delete the weights on the left
                    model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin(), model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n*X));



                }
            }

            if(n==(total_nodes-1)){ // if it is the last node
                // calculate the number of output channels computed for the last node
                // for e.g. if X=3 i.e. we compute 3 output channel pert node and M=8...so we have total_nodes=3...but the last node would compute only 2 output channel
                // The output channel(Y) for the last node can be calculated as: M-(total_nodes-1)*X. This will hold true for both symmetric and non symmetric cases
                auto Y= M-(total_nodes-1)*X; // no of output channels computed at the last node
                model_modify->subgraphs[0]->tensors[tensor_weights]->shape[0]=Y;// Modify the output shape of the weights tensor
                model_modify->subgraphs[0]->tensors[tensor_bias]->shape[0]=Y;// Modify the output shape of the bias tensor
                model_modify->subgraphs[0]->tensors[tensor_output]->shape[3]=Y; // Modify the output shape of the output tensor
                // erase the non needed weight
                model_modify->buffers[buffer_tensor_weights].get()->data.erase(model_modify->buffers[buffer_tensor_weights].get()->data.begin(), model_modify->buffers[buffer_tensor_weights].get()->data.begin()+(n)*w);
                // erase the non needed biases
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin(), model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n*X));


            }
            flatbuffers::FlatBufferBuilder fbb1;
            fbb1.Finish(CreateModel(fbb1, model_modify.get(), &rehasher),
                        tflite::ModelIdentifier());
            auto len1 = fbb1.GetSize(); // get the length of the flatbuffer
            // printf("size of the modified buffer is: %d\n",len1);
            char *buf_modified = (char *)malloc(len1); // Generate a char buff array to store the data using the length of flatbuffers
            memcpy(buf_modified,fbb1.GetBufferPointer(),len1); // copying the flatbuffer data into the generated character buffer

            std::ofstream outfile;
            //       std::string name;
//        if(k==0)
//            name = "model_conv3_source_node_split_"+std::to_string(k)+".tflite";
//        else
//            name = "model_conv3_worker_node_split_"+std::to_string(k)+".tflite";

            // save the partitioned models by name of model_output_split_partition_%
            std::string name = "layer_"+to_string(layer_index)+"_partition_"+std::to_string(n)+".tflite";

            outfile.open(name, std::ios::binary | std::ios::out); // save the model in a file
            outfile.write(buf_modified,len1);
            outfile.close();
            free(buf_modified);



        }

        free(buf);
        printf("\nAutomated output splitting of weights of convolution layer completed successfully...\n");






    }
    else if(code==4 ){ // layer is Depthwise Conv and has only one operator

//        printf("weights\n");
//        // print the weights
//        for(auto itr=model1->buffers[buffer_tensor_weights].get()->data.begin();itr!=model1->buffers[buffer_tensor_weights].get()->data.end();itr++){
//            printf("%d,\n",*itr);
//        }

        //printf("Q: %d\n",model1->subgraphs[0]->tensors[tensor_weights]->quantization->scale.size());

        double X= no_of_output_channel_per_node; // no of output channels to compute per node

        if(X==N){ // that means no output splitting is neeeded
            printf("No output splitting needed for model: %s\n",model_name.c_str());
            return;
        }

        // find out the total number of nodes depending upon the value of X. for depthwise convolution this will depend on X
        int total_nodes = ceil((N/X));
        //printf("total nodes: %d\n",total_nodes);

        // except the last node each of the (total_nodes-1) nodes would have R*S*M*X weights for depthwise conv,  which we denote by small w
        //auto w=R*S*M*X;



        for(int n=0;n<total_nodes;n++) {
            auto model_modify = tflite::UnPackModel(buf,
                                                    &resolver); // Unpack the buf into another model for modification
            //if(n<(total_nodes-1)){ // nodes except the last node

            model_modify->subgraphs[0]->tensors[tensor_weights]->shape[3] = X;// Modify the output shape of the weights tensor...index 3 for depthwise convolution
            model_modify->subgraphs[0]->tensors[tensor_bias]->shape[0] = X;// Modify the output shape of the bias tensor
            model_modify->subgraphs[0]->tensors[tensor_output]->shape[3] = X; // Modify the output shape of the output tensor
            model_modify->subgraphs[0]->tensors[tensor_input]->shape[3] = X; // Modify the output shape of the input tensor

            // first modify the weight vectors
            vector<uint8_t> weights= model_modify->buffers[buffer_tensor_weights].get()->data;

            vector<uint8_t> temp1;
            vector<uint8_t> temp2;
            temp1=extract_vec(weights,0,N-1);
            if(n!=(total_nodes-1)){ // if not the last node
                temp2=extract_vec(temp1,n*X,(n+1)*X-1);
            }
            else{ // they are the last node
                int Y= N-(total_nodes-1)*X;
                temp2=extract_vec(temp1,n*X,N-1);
//            if(Y!=X) // if X==Y
//                //temp2=extract_vec(temp1,n*X,((n+1)*Y+1)); // prev code
//                temp2=extract_vec(temp1,n*X,N); //nX to N-Y
//            else
//                temp2=extract_vec(temp1,n*X,((n+1)*X-1));
            }

            for(int i=1;i<=(M*R*S-1);i++){ // was i<=N
                vector<uint8_t> temp1_m;
                vector<uint8_t> temp2_m;

                //temp1_m=extract_vec(weights,8*i,8*(i+1)-1);
                temp1_m=extract_vec(weights,N*i,N*(i+1)-1);
                if(n!=(total_nodes-1)){ // if not the last node
                    temp2_m=extract_vec(temp1_m,n*X,(n+1)*X-1);
                    concat_vec(temp2,temp2_m);
                }
                else{ // they are the last node
                    int Y= N-(total_nodes-1)*X;
                    temp2=extract_vec(temp1_m,n*X,N-1);
//                    if(Y!=X) // if X==Y
//                        temp2_m=extract_vec(temp1_m,n*X,(n+1)*Y+1);
//                    else
//                        temp2_m=extract_vec(temp1_m,n*X,(n+1)*X-1);
                    concat_vec(temp2,temp2_m);
                }

            }

            model_modify->buffers[buffer_tensor_weights]->data.clear(); // clear the weight data

            // push back the weight data
            for (auto itr=temp2.begin();itr!=temp2.end();itr++){
                model_modify->buffers[buffer_tensor_weights]->data.push_back(*itr);
            }


            // Modify the biases as well

            if(n==0){ // if first node

                // erase the non needed biases
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*X, model_modify->buffers[buffer_tensor_bias].get()->data.end());


            }
            else if(n>0 && (n!=(total_nodes-1))){ // if not the first node and not the last node

                // first erase the biases on the right
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n+1)*X, model_modify->buffers[buffer_tensor_bias].get()->data.end());
                // them delete the biases on the left
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin(), model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n*X));

            }

            if(n==(total_nodes-1)){ // if it is the last node
                // calculate the number of output channels computed for the last node
                // for e.g. if X=3 i.e. we compute 3 output channel pert node and M=8...so we have total_nodes=3...but the last node would compute only 2 output channel
                // The output channel(Y) for the last node can be calculated as: M-(total_nodes-1)*X. This will hold true for both symmetric and non symmetric cases
                auto Y= N-(total_nodes-1)*X; // no of output channels computed at the last node
                model_modify->subgraphs[0]->tensors[tensor_weights]->shape[3]=Y;// Modify the output shape of the weights tensor. 3 for depthwise convolution
                model_modify->subgraphs[0]->tensors[tensor_bias]->shape[0]=Y;// Modify the output shape of the bias tensor
                model_modify->subgraphs[0]->tensors[tensor_output]->shape[3]=Y; // Modify the output shape of the output tensor
                model_modify->subgraphs[0]->tensors[tensor_input]->shape[3]=Y; // Modify the output shape of the input tensor

                // erase the non needed biases
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin(), model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n*X));


            }


            flatbuffers::FlatBufferBuilder fbb1;
            fbb1.Finish(CreateModel(fbb1, model_modify.get(), &rehasher),
                        tflite::ModelIdentifier());
            auto len1 = fbb1.GetSize(); // get the length of the flatbuffer
            // printf("size of the modified buffer is: %d\n",len1);
            char *buf_modified = (char *)malloc(len1); // Generate a char buff array to store the data using the length of flatbuffers
            memcpy(buf_modified,fbb1.GetBufferPointer(),len1); // copying the flatbuffer data into the generated character buffer

            std::ofstream outfile;
            //       std::string name;
//        if(k==0)
//            name = "model_conv3_source_node_split_"+std::to_string(k)+".tflite";
//        else
//            name = "model_conv3_worker_node_split_"+std::to_string(k)+".tflite";

            // save the partitioned models by name of model_output_split_partition_%
            std::string name = "layer_"+to_string(layer_index)+"_partition_"+std::to_string(n)+".tflite";

            outfile.open(name, std::ios::binary | std::ios::out); // save the model in a file
            outfile.write(buf_modified,len1);
            outfile.close();
            free(buf_modified);



        }

        free(buf);
        printf("\nAutomated output splitting of weights of depthwise convolution layer completed successfully...\n");



    }
//    else if(code==3 && model1->subgraphs[0]->operators.size()==2)// layer is Conv2D and has only two operator. The second one being avgpool2d
//    {
//
//    }
    else{ // layer is neither of two
        printf("Error! Layer is neither Convolution or Depthwise Convolution\n");
    }


}



//  This script takes in an input model: convolution or depthwise convolution and then incrementally saves the output partitions from 1 to max possible channels
void conv_depthwise_conv_output_split_sequential(string model_name, double no_of_output_channel_per_node){ // where M are the number of output channels computed per node

    // Read file into a malloced byte array
    FILE *f = fopen(model_name.c_str(), "rb"); // input the tflite model to split
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buf = (char *)malloc(fsize);
    fread(buf, fsize, 1, f);
    fclose(f);

    auto model = tflite::GetMutableModel(buf); // to get the mutable model from the buffer

    auto resolver = flatbuffers::resolver_function_t( // Get the resolver used to unpack the buffer into another model for modifying it
            [](void **pointer_adr, flatbuffers::hash_value_t hash) {
                (void)pointer_adr;
                (void)hash;
                // Don't actually do anything, leave variable null.
            });

    auto rehasher = flatbuffers::rehasher_function_t( // Get the rehasher used to create a flatbuffer latter
            [](void *pointer) -> flatbuffers::hash_value_t {
                (void)pointer;
                return 0;
            });
    //printf("\nExecuting automated weight splitting of the provided layer...\n");
    // Since we are partitioning only a single convolution layer, we will have only a single convolution operation
    // A single convolution operation will have three input tensors (input, weights and biases) and one output tensor (output)
    // Each of these tensors will point to a buffer in the model that will store actual weights/activations corresponding to the tensor
    // First we have to find the indexes of the tensors and buffers they point for the corresponding (input, weights, bias and output)
    // We leverage the property that the operator stores input, weights, biases and output in order.
    // For e.g. model1->subgraphs[0]->operators[0]->inputs[0] will always be input tensors, model1->subgraphs[0]->operators[0]->inputs[1] will always be the weights tensor and model1->subgraphs[0]->operators[0]->inputs[2] will always be the bias tensor

    auto model1 = tflite::UnPackModel(buf, &resolver); // Unpack the buf into another model for modification


    auto tensor_input= model1->subgraphs[0]->operators[0]->inputs[0];
    auto buffer_tensor_input= model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->inputs[0]]->buffer;

    auto tensor_weights= model1->subgraphs[0]->operators[0]->inputs[1];
    auto buffer_tensor_weights= model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->inputs[1]]->buffer;

    auto tensor_bias= model1->subgraphs[0]->operators[0]->inputs[2];
    auto buffer_tensor_bias= model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->inputs[2]]->buffer;

    auto tensor_output= model1->subgraphs[0]->operators[0]->outputs[0];
    auto buffer_tensor_output= model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->outputs[0]]->buffer;

    // Figure out the values of weight dimensions R and S and output channel M and input channel N
    auto M = model1->subgraphs[0]->tensors[tensor_weights]->shape[0];
    auto R = model1->subgraphs[0]->tensors[tensor_weights]->shape[1];
    auto S = model1->subgraphs[0]->tensors[tensor_weights]->shape[2];
    auto N = model1->subgraphs[0]->tensors[tensor_weights]->shape[3];

//    printf("%d\n",M);
//    printf("%d\n",R);
//    printf("%d\n",S);
//    printf("%d\n",N);

    // Here W will contain the total weights in bytes / total number of weights to split among the nodes "Nodes"
    int W = model1->buffers[buffer_tensor_weights]->data.size();  // so the weights would be indexed from 0 to W-1



    // Detect if the layer is Conv2D or Depthwise
    auto opocode_index_current= model1->subgraphs[0]->operators[0]->opcode_index;
    auto code= model1->operator_codes[opocode_index_current]->builtin_code;


    if(code==3 )// layer is Conv2D and has only one operator
    {
        double X= no_of_output_channel_per_node; // no of output channels to compute per node

        if(X==M){ // that means no output splitting is neeeded
            printf("No output splitting needed for model: %s\n",model_name.c_str());
            return;
        }

        // find out the total number of nodes depending upon the value of X
        // find out the total number of nodes depending upon the value of X
        int total_nodes = ceil((M/X));
        //printf("total nodes: %d\n",total_nodes);

        // except the last node each of the (total_nodes-1) nodes would have R*S*N*X weights which we denote by small w
        auto w=R*S*N*X;

        // so except the last node for each node n where n goes from 0 to (total_nodes-1), we needs to keep the weights from index: [nw,(n+1)*w-1]
        // for the last node we need to keep the weights from [(n+1)*w, W-1]
        // so for ach node n where n goes from 0 to (total_nodes-2), we needs to erase the on required partitions i.e. [0,nw-1] and [(n+1)*w,W-1]
        // for last node where n= (total_nodes-1), we need to erase the partition from [0,(n+1)*w-1]
        for(int n=0;n<total_nodes;n++){
            auto model_modify = tflite::UnPackModel(buf, &resolver); // Unpack the buf into another model for modification
            if(n<(total_nodes-1)){ // nodes except the last node

                model_modify->subgraphs[0]->tensors[tensor_weights]->shape[0]=X;// Modify the output shape of the weights tensor
                model_modify->subgraphs[0]->tensors[tensor_bias]->shape[0]=X;// Modify the output shape of the bias tensor
                model_modify->subgraphs[0]->tensors[tensor_output]->shape[3]=X; // Modify the output shape of the output tensor


                if(n==0){ // if first node
                    // erase the non needed weight
                    model_modify->buffers[buffer_tensor_weights].get()->data.erase(model_modify->buffers[buffer_tensor_weights].get()->data.begin()+w, model_modify->buffers[buffer_tensor_weights].get()->data.end());
                    // erase the non needed biases
                    model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*X, model_modify->buffers[buffer_tensor_bias].get()->data.end());


                }
                else{ // if not the first node
                    // first erase the weights on the right
                    model_modify->buffers[buffer_tensor_weights].get()->data.erase(model_modify->buffers[buffer_tensor_weights].get()->data.begin()+(n+1)*w, model_modify->buffers[buffer_tensor_weights].get()->data.end());
                    // them delete the weights on the left
                    model_modify->buffers[buffer_tensor_weights].get()->data.erase(model_modify->buffers[buffer_tensor_weights].get()->data.begin(), model_modify->buffers[buffer_tensor_weights].get()->data.begin()+n*w);

                    // do similar erase for the biases
                    // first erase the weights on the right
                    model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n+1)*X, model_modify->buffers[buffer_tensor_bias].get()->data.end());
                    // them delete the weights on the left
                    model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin(), model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n*X));



                }
            }

            if(n==(total_nodes-1)){ // if it is the last node
                // calculate the number of output channels computed for the last node
                // for e.g. if X=3 i.e. we compute 3 output channel pert node and M=8...so we have total_nodes=3...but the last node would compute only 2 output channel
                // The output channel(Y) for the last node can be calculated as: M-(total_nodes-1)*X. This will hold true for both symmetric and non symmetric cases
                auto Y= M-(total_nodes-1)*X; // no of output channels computed at the last node
                model_modify->subgraphs[0]->tensors[tensor_weights]->shape[0]=Y;// Modify the output shape of the weights tensor
                model_modify->subgraphs[0]->tensors[tensor_bias]->shape[0]=Y;// Modify the output shape of the bias tensor
                model_modify->subgraphs[0]->tensors[tensor_output]->shape[3]=Y; // Modify the output shape of the output tensor
                // erase the non needed weight
                model_modify->buffers[buffer_tensor_weights].get()->data.erase(model_modify->buffers[buffer_tensor_weights].get()->data.begin(), model_modify->buffers[buffer_tensor_weights].get()->data.begin()+(n)*w);
                // erase the non needed biases
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin(), model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n*X));


            }
            flatbuffers::FlatBufferBuilder fbb1;
            fbb1.Finish(CreateModel(fbb1, model_modify.get(), &rehasher),
                        tflite::ModelIdentifier());
            auto len1 = fbb1.GetSize(); // get the length of the flatbuffer
            if(n==0)
                printf("%d,\n",len1);
            char *buf_modified = (char *)malloc(len1); // Generate a char buff array to store the data using the length of flatbuffers
            memcpy(buf_modified,fbb1.GetBufferPointer(),len1); // copying the flatbuffer data into the generated character buffer

            std::ofstream outfile;
            //       std::string name;
//        if(k==0)
//            name = "model_conv3_source_node_split_"+std::to_string(k)+".tflite";
//        else
//            name = "model_conv3_worker_node_split_"+std::to_string(k)+".tflite";

            // save only the first node partitioned models by name of model_output_split_partition_%
            if(n==0){
                std::string name = model_name+"_output_split_partition_"+"with_out_channel_"+std::to_string(int(no_of_output_channel_per_node))+".tflite";

                outfile.open(name, std::ios::binary | std::ios::out); // save the model in a file
                outfile.write(buf_modified,len1);
                outfile.close();
            }

            free(buf_modified);



        }

        free(buf);
        // printf("\nAutomated output splitting of weights of convolution layer completed successfully...\n");






    }
    else if(code==4 ){ // layer is Depthwise Conv and has only one operator

//        printf("weights\n");
//        // print the weights
//        for(auto itr=model1->buffers[buffer_tensor_weights].get()->data.begin();itr!=model1->buffers[buffer_tensor_weights].get()->data.end();itr++){
//            printf("%d,\n",*itr);
//        }

        //printf("Q: %d\n",model1->subgraphs[0]->tensors[tensor_weights]->quantization->scale.size());

        double X= no_of_output_channel_per_node; // no of output channels to compute per node

        if(X==N){ // that means no output splitting is neeeded
            printf("No output splitting needed for model: %s\n",model_name.c_str());
            return;
        }

        // find out the total number of nodes depending upon the value of X. for depthwise convolution this will depend on X
        int total_nodes = ceil((N/X));
        //printf("total nodes: %d\n",total_nodes);

        // except the last node each of the (total_nodes-1) nodes would have R*S*M*X weights for depthwise conv,  which we denote by small w
        //auto w=R*S*M*X;



        for(int n=0;n<total_nodes;n++) {
            auto model_modify = tflite::UnPackModel(buf,
                                                    &resolver); // Unpack the buf into another model for modification
            //if(n<(total_nodes-1)){ // nodes except the last node

            model_modify->subgraphs[0]->tensors[tensor_weights]->shape[3] = X;// Modify the output shape of the weights tensor...index 3 for depthwise convolution
            model_modify->subgraphs[0]->tensors[tensor_bias]->shape[0] = X;// Modify the output shape of the bias tensor
            model_modify->subgraphs[0]->tensors[tensor_output]->shape[3] = X; // Modify the output shape of the output tensor
            model_modify->subgraphs[0]->tensors[tensor_input]->shape[3] = X; // Modify the output shape of the input tensor

            // first modify the weight vectors
            vector<uint8_t> weights= model_modify->buffers[buffer_tensor_weights].get()->data;

            vector<uint8_t> temp1;
            vector<uint8_t> temp2;
            temp1=extract_vec(weights,0,N-1);
            if(n!=(total_nodes-1)){ // if not the last node
                temp2=extract_vec(temp1,n*X,(n+1)*X-1);
            }
            else{ // they are the last node
                int Y= N-(total_nodes-1)*X;
                temp2=extract_vec(temp1,n*X,N-1);
//                if(Y!=X) // if X==Y
//                    temp2=extract_vec(temp1,n*X,((n+1)*Y+1));
//                else
//                    temp2=extract_vec(temp1,n*X,((n+1)*X-1));
            }

            for(int i=1;i<=(M*R*S-1);i++){ // was i<=N
                vector<uint8_t> temp1_m;
                vector<uint8_t> temp2_m;

                //temp1_m=extract_vec(weights,8*i,8*(i+1)-1);
                temp1_m=extract_vec(weights,N*i,N*(i+1)-1);
                if(n!=(total_nodes-1)){ // if not the last node
                    temp2_m=extract_vec(temp1_m,n*X,(n+1)*X-1);
                    concat_vec(temp2,temp2_m);
                }
                else{ // they are the last node
                    int Y= N-(total_nodes-1)*X;
                    temp2=extract_vec(temp1_m,n*X,N-1);
//                    if(Y!=X) // if X==Y
//                        temp2_m=extract_vec(temp1_m,n*X,(n+1)*Y+1);
//                    else
//                        temp2_m=extract_vec(temp1_m,n*X,(n+1)*X-1);
                    concat_vec(temp2,temp2_m);
                }

            }

            model_modify->buffers[buffer_tensor_weights]->data.clear(); // clear the weight data

            // push back the weight data
            for (auto itr=temp2.begin();itr!=temp2.end();itr++){
                model_modify->buffers[buffer_tensor_weights]->data.push_back(*itr);
            }


            // Modify the biases as well

            if(n==0){ // if first node

                // erase the non needed biases
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*X, model_modify->buffers[buffer_tensor_bias].get()->data.end());


            }
            else if(n>0 && (n!=(total_nodes-1))){ // if not the first node and not the last node

                // first erase the biases on the right
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n+1)*X, model_modify->buffers[buffer_tensor_bias].get()->data.end());
                // them delete the biases on the left
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin(), model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n*X));

            }

            if(n==(total_nodes-1)){ // if it is the last node
                // calculate the number of output channels computed for the last node
                // for e.g. if X=3 i.e. we compute 3 output channel pert node and M=8...so we have total_nodes=3...but the last node would compute only 2 output channel
                // The output channel(Y) for the last node can be calculated as: M-(total_nodes-1)*X. This will hold true for both symmetric and non symmetric cases
                auto Y= N-(total_nodes-1)*X; // no of output channels computed at the last node
                model_modify->subgraphs[0]->tensors[tensor_weights]->shape[3]=Y;// Modify the output shape of the weights tensor. 3 for depthwise convolution
                model_modify->subgraphs[0]->tensors[tensor_bias]->shape[0]=Y;// Modify the output shape of the bias tensor
                model_modify->subgraphs[0]->tensors[tensor_output]->shape[3]=Y; // Modify the output shape of the output tensor
                model_modify->subgraphs[0]->tensors[tensor_input]->shape[3]=Y; // Modify the output shape of the input tensor

                // erase the non needed biases
                model_modify->buffers[buffer_tensor_bias].get()->data.erase(model_modify->buffers[buffer_tensor_bias].get()->data.begin(), model_modify->buffers[buffer_tensor_bias].get()->data.begin()+4*(n*X));


            }


            flatbuffers::FlatBufferBuilder fbb1;
            fbb1.Finish(CreateModel(fbb1, model_modify.get(), &rehasher),
                        tflite::ModelIdentifier());
            auto len1 = fbb1.GetSize(); // get the length of the flatbuffer
            if(n==0)
                printf("%d,\n",len1);
            char *buf_modified = (char *)malloc(len1); // Generate a char buff array to store the data using the length of flatbuffers
            memcpy(buf_modified,fbb1.GetBufferPointer(),len1); // copying the flatbuffer data into the generated character buffer

            std::ofstream outfile;
            //       std::string name;
//        if(k==0)
//            name = "model_conv3_source_node_split_"+std::to_string(k)+".tflite";
//        else
//            name = "model_conv3_worker_node_split_"+std::to_string(k)+".tflite";

            // save only the partitioned model of first node by name of model_output_split_partition_%
            if(n==0){
                std::string name = model_name+"_output_split_partition_"+"with_out_channel_"+std::to_string(int(no_of_output_channel_per_node))+".tflite";

                outfile.open(name, std::ios::binary | std::ios::out); // save the model in a file
                outfile.write(buf_modified,len1);
                outfile.close();

            }
            free(buf_modified);





        }

        free(buf);
        //printf("\nAutomated output splitting of weights of depthwise convolution layer completed successfully...\n");



    }
//    else if(code==3 && model1->subgraphs[0]->operators.size()==2)// layer is Conv2D and has only two operator. The second one being avgpool2d
//    {
//
//    }
    else{ // layer is neither of two
        printf("Error! Layer is neither Convolution or Depthwise Convolution\n");
    }


}




double get_N_or_M(string model_name) { // it returns the input/ output channel for a layer depending upon if the layer is conv or depthwise conv

    // Read file into a malloced byte array
    FILE *f = fopen(model_name.c_str(), "rb"); // input the tflite model to split
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buf = (char *) malloc(fsize);
    fread(buf, fsize, 1, f);
    fclose(f);

    auto model = tflite::GetMutableModel(buf); // to get the mutable model from the buffer

    auto resolver = flatbuffers::resolver_function_t( // Get the resolver used to unpack the buffer into another model for modifying it
            [](void **pointer_adr, flatbuffers::hash_value_t hash) {
                (void) pointer_adr;
                (void) hash;
                // Don't actually do anything, leave variable null.
            });

    auto rehasher = flatbuffers::rehasher_function_t( // Get the rehasher used to create a flatbuffer latter
            [](void *pointer) -> flatbuffers::hash_value_t {
                (void) pointer;
                return 0;
            });
    printf("\nExecuting automated weight splitting of the provided layer...\n");
    // Since we are partitioning only a single convolution layer, we will have only a single convolution operation
    // A single convolution operation will have three input tensors (input, weights and biases) and one output tensor (output)
    // Each of these tensors will point to a buffer in the model that will store actual weights/activations corresponding to the tensor
    // First we have to find the indexes of the tensors and buffers they point for the corresponding (input, weights, bias and output)
    // We leverage the property that the operator stores input, weights, biases and output in order.
    // For e.g. model1->subgraphs[0]->operators[0]->inputs[0] will always be input tensors, model1->subgraphs[0]->operators[0]->inputs[1] will always be the weights tensor and model1->subgraphs[0]->operators[0]->inputs[2] will always be the bias tensor

    auto model1 = tflite::UnPackModel(buf, &resolver); // Unpack the buf into another model for modification



    auto tensor_weights = model1->subgraphs[0]->operators[0]->inputs[1];
    auto buffer_tensor_weights = model1->subgraphs[0]->tensors[model1->subgraphs[0]->operators[0]->inputs[1]]->buffer;


    // Figure out the values of weight dimensions R and S and output channel M and input channel N
    auto M = model1->subgraphs[0]->tensors[tensor_weights]->shape[0];
    auto R = model1->subgraphs[0]->tensors[tensor_weights]->shape[1];
    auto S = model1->subgraphs[0]->tensors[tensor_weights]->shape[2];
    auto N = model1->subgraphs[0]->tensors[tensor_weights]->shape[3];



    // Detect if the layer is Conv2D or Depthwise
    auto opocode_index_current = model1->subgraphs[0]->operators[0]->opcode_index;
    auto code = model1->operator_codes[opocode_index_current]->builtin_code;

    if(code==3){ // if conv: return M
        return M;
    }
    else if(code==4){ // if depthwise conv: return N
        return  N;
    }
    else{
        printf("Error: Layer neither convolution and nor depthwise convolution");
    }
}






// To get the mutable model use all the mutable API's
