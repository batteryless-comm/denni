// Automated Script for detecting the intermediate tensor size and padding while backtracking from a given output pixel or a group of pixel to the initial input of a provided subgraph

/*
-----------------------------------------------------
| Created on: January 2, 2021
| Author: Rohit Sahu
|
| Department of Electrical and Computer Engineering
| Iowa State University
-----------------------------------------------------
*/


// Include the necessary files
#include "flatbuffers/flatbuffers.h"
#include "schema_generated.h"
#include <fstream>   // C++ header file for file access
#include <iostream>  // C++ header file for printing
#include <cstdio>
#include "flatbuffers/idl.h"
#include "flatbuffers/registry.h"
#include "flatbuffers/util.h"
using namespace std;

// function to find max between two integers
int max_int(int a, int b){
    if(a>=b){
        return a;
    }
    else{
        return b;
    }
}

// function to find min between two integers
int min_int(int a, int b){
    if(a>=b){
        return b;
    }
    else{
        return a;
    }
}


void Add_PADV2(shared_ptr<tflite::ModelT> model_base,int l, int left_pad, int right_pad, int top_pad, int bottom_pad ) {

    // First Load the base model in which we need to insert the PAD operator. update this if the base model changes
    FILE *f1 = fopen("model_baseline_cross_fusion.tflite", "rb");
    fseek(f1, 0, SEEK_END);
    long f1size = ftell(f1);
    fseek(f1, 0, SEEK_SET);
    char *buf1 = (char *) malloc(f1size);
    fread(buf1, f1size, 1, f1);
    fclose(f1);

    // Second Load the ref model which already has an existing PADV2 operator
    FILE *f = fopen("ref_model.tflite", "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buf = (char *) malloc(fsize);
    fread(buf, fsize, 1, f);
    fclose(f);


    // Create the resolver and hasher to unpack the models into variables


    auto resolver = flatbuffers::resolver_function_t( // Get the resolver used to unpack the buffer into another model for modifying it
            [](void **pointer_adr, flatbuffers::hash_value_t hash) {
                (void) pointer_adr;
                (void) hash;
                // Don't actually do anything, leave variable null.
            });


    auto rehasher = flatbuffers::rehasher_function_t( // Get the rehasher used to create a flatbuffer latter
            [](void *pointer) -> flatbuffers::hash_value_t {
                (void) pointer;
                return 0;
            });


    // Unpack the base model and its clone into variables

//    auto model_base = tflite::UnPackModel(buf1, &resolver);

    auto model_clone = tflite::UnPackModel(buf1, &resolver);

    auto model_ref = tflite::UnPackModel(buf, &resolver);

    // First add a new output tensor after the PADV2 operator and add a new empty buffer and point tensor to it

    // Depending upon the layer/op index "l" figure out the output tensor in the clone model of the op/ layer.
    // This will added to the base model to become the new output tensor of the pad operator.

    // First we need to figure out the output tensor index of the layer/op "l"
    auto tensor_output_index = model_clone->subgraphs[0]->operators[l]->outputs[0];

    // Then we need to figure out the buffer index pointed by the output tensor the layer/op "l"
    auto buffer_output_index = model_clone->subgraphs[0]->tensors[tensor_output_index]->buffer;


    // Using the output tensor index we need to find the actual tensor to insert in the base model. Since every data structure is a unique pointer, we need to move the actual
    // object from the clone model to the base model. This will delete the object in the clone model

    model_base->subgraphs[0]->tensors.insert(model_base->subgraphs[0]->tensors.end(),
                                             std::move(model_clone->subgraphs[0]->tensors[tensor_output_index]));

    // For storing the current shape before padding
    int shape1= model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[1];
    int shape2= model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[2];

    //  Now update the shape of the tensor depending upon the padding used. The tensor added will be the last tensor
    model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[1] = shape1+ left_pad + right_pad;
    model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[2] = shape2 + top_pad + bottom_pad;

    // for checking if the shape is coming correctly
//    int shape1m= model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[1];
//    int shape2m= model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[2];


    // Add the name of the newly added tensor. so that it's not null
    int r= rand()%100;
    string tensor_name = "pad"+ std::to_string(r);
    model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->name = tensor_name;


    // Second add a new empty buffer for the above newly added tensor from the clone model to the base model
    model_base->buffers.insert(model_base->buffers.end(), std::move(model_clone->buffers[buffer_output_index]));

    // Now we need to point the newly added tensor to the above buffer. The newly added buffer would be the last one.
    model_base->subgraphs[0]->tensors[tensor_output_index]->buffer = model_base->buffers.size() - 1;


    // Now we need to add the PADV2 layer/op from the ref model into base model after the lth layer in the inference subgraph
    // if the base model has a single layer, then add the new operator at the end. if there are multiple operators ...then add the new operators after the layer l
    if(model_base->subgraphs[0]->operators.size()==1){
        model_base->subgraphs[0]->operators.insert(model_base->subgraphs[0]->operators.end(),
                                                   std::move(model_ref->subgraphs[0]->operators[0]));
    }
    else{
        model_base->subgraphs[0]->operators.insert(model_base->subgraphs[0]->operators.begin()+l+1,
                                                   std::move(model_ref->subgraphs[0]->operators[0]));
    }


    // Since the operator table references the operator_codes as a look up table, we need to add the code for the PAD operator
    model_base->operator_codes.insert(model_base->operator_codes.end(), std::move(model_clone->operator_codes[0]));

    //Change the built in code fpr PADv2 which has BuiltinOperator enum code: 60 in the newly inserted operator code
    model_base->operator_codes[model_base->operator_codes.size() -
                               1]->builtin_code = static_cast<tflite::BuiltinOperator>(60); // Pad operator

    // Now we have to point the newly inserted operator to the newly inserted operator code "index" which would the last one
    model_base->subgraphs[0]->operators[l + 1]->opcode_index = model_base->operator_codes.size() - 1;

    // Add input tensor to the newly created PADV2 operator at l+1 as the output of the previous operator at l
    // model_base->subgraphs[0]->operators[l + 1]->inputs[0] = model_base->subgraphs[0]->operators[l]->outputs[0];
    model_base->subgraphs[0]->operators[l + 1]->inputs[0] = tensor_output_index;

    // Add output tensor to the newly created PADV2 operator at l+1 as the newly inserted tensor in the base model
    model_base->subgraphs[0]->operators[l + 1]->outputs[0] = model_base->subgraphs[0]->tensors.size() - 1;

    // Add this tensor as input to the next operator. if it is not the last tensor
    //  if((l+1)!=(model_base->subgraphs[0]->operators.size()-1)) // if the PAD tensor is not the last tensor
    //model_base->subgraphs[0]->operators[l + 2]->inputs[0] = model_base->subgraphs[0]->tensors.size() - 1;

    model_base->subgraphs[0]->operators[l + 2]->inputs[0] =  model_base->subgraphs[0]->operators[l + 1]->outputs[0];



    // If the added PADV2 operator is the last op/layer, we need to change the final output tensor of the whole inference graph
    if ((l + 1) == (model_base->subgraphs[0]->operators.size() - 1)) {
        *model_base->subgraphs[0]->outputs.data() = model_base->subgraphs[0]->tensors.size() - 1;
    }

    // modify the padding buffer 10 of model_ref according to the padding scheme used
    int i = 0;
    for (auto itr = model_ref->buffers[10]->data.begin(); itr != model_ref->buffers[10]->data.end(); itr = itr + 4) {
        if (i == 2) {
            *itr = top_pad;
        }
        if (i == 3) {
            *itr = bottom_pad;

        }
        if (i == 4) {
            *itr = left_pad;
        }
        if (i == 5) {
            *itr = right_pad;
        }
        if(i==0||i==1||i==6||i==7){
            *itr = 0;
        }
        i++;
    }

    // Add the modified padding buffer to the base model
    model_base->buffers.insert(model_base->buffers.end(), std::move(model_ref->buffers[10]));

    // Add the padding tensor which would point to the newly added buffer
    model_base->subgraphs[0]->tensors.insert(model_base->subgraphs[0]->tensors.end(),
                                             std::move(model_ref->subgraphs[0]->tensors[9]));

    // Provide name to the newly added tensor
    // model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->name = tensor_name;

    // point the newly added tensor's buffer  to the newly added buffer. Both buffer and tensors would be the last elements
    model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->buffer = model_base->buffers.size() - 1;

    // Add the newly added tensor as an input to the PAD operator
    model_base->subgraphs[0]->operators[l + 1]->inputs[1] = model_base->subgraphs[0]->tensors.size() - 1;



    // Save the modified model
//    // Re-serialize the data.
//    flatbuffers::FlatBufferBuilder fbb1;
//    fbb1.Finish(CreateModel(fbb1, model_base.get(), &rehasher),
//                tflite::ModelIdentifier());
//    auto len1 = fbb1.GetSize(); // get the length of the flatbuffer
//    printf("size of the modified buffer is: %d\n",len1);
//    char *buf_modified = (char *)malloc(len1); // Generate a char buff array to store the data using the length of flatbuffers
//    memcpy(buf_modified,fbb1.GetBufferPointer(),len1); // copying the flatbuffer data into the generated character buffer
//    std::ofstream outfile;
//    // std::string name = "model_pad.tflite";
//    outfile.open("model_pad_modified.tflite", std::ios::binary | std::ios::out); // save the model in a file
//    outfile.write(buf_modified,len1);
//    outfile.close();
//
//    // free the dynamically allocated data
//
//    free(buf_modified);




    free(buf);

    free(buf1);





}


void Add_PADV2_top(shared_ptr<tflite::ModelT> model_base,int l, int left_pad, int right_pad, int top_pad, int bottom_pad ) { // code to add the PADV2 operator at the top of the network i.e. before layer 0

    // First Load the base model in which we need to insert the PAD operator. update this if the base model changes
    FILE *f1 = fopen("model_split.tflite", "rb");
    fseek(f1, 0, SEEK_END);
    long f1size = ftell(f1);
    fseek(f1, 0, SEEK_SET);
    char *buf1 = (char *) malloc(f1size);
    fread(buf1, f1size, 1, f1);
    fclose(f1);

    // Second Load the ref model which already has an existing PADV2 operator
    FILE *f = fopen("ref_model.tflite", "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buf = (char *) malloc(fsize);
    fread(buf, fsize, 1, f);
    fclose(f);


    // Create the resolver and hasher to unpack the models into variables


    auto resolver = flatbuffers::resolver_function_t( // Get the resolver used to unpack the buffer into another model for modifying it
            [](void **pointer_adr, flatbuffers::hash_value_t hash) {
                (void) pointer_adr;
                (void) hash;
                // Don't actually do anything, leave variable null.
            });


    auto rehasher = flatbuffers::rehasher_function_t( // Get the rehasher used to create a flatbuffer latter
            [](void *pointer) -> flatbuffers::hash_value_t {
                (void) pointer;
                return 0;
            });


    // Unpack the base model and its clone into variables

//    auto model_base = tflite::UnPackModel(buf1, &resolver);

    auto model_clone = tflite::UnPackModel(buf1, &resolver);

    auto model_ref = tflite::UnPackModel(buf, &resolver);

    // debug code:

    for(auto itr=model_base->subgraphs[0]->operators.begin();itr!=model_base->subgraphs[0]->operators.end();itr++){
        printf("input size : %d\n",itr->get()->inputs.size());

        for(int i=0;i<itr->get()->inputs.size();i++){
            printf("input tensor : %d\n",itr->get()->inputs[i]);
        }
    }

    for(auto itr=model_base->subgraphs[0]->tensors.begin();itr!=model_base->subgraphs[0]->tensors.end();itr++){
        printf("%s\n",itr->get()->name.c_str());
        printf("%d\n",itr->get()->buffer);
    }


    // First add a new output tensor after the PADV2 operator and add a new empty buffer and point tensor to it

    // Depending upon the layer/op index "l" figure out the output tensor in the clone model of the op/ layer.
    // This will added to the base model to become the new output tensor of the pad operator.

    // First we need to figure out the input tensor index of the layer/op "l"... which is the input of the first layer i.e. l=0
    auto tensor_input_index = model_clone->subgraphs[0]->operators[l]->inputs[0];

    // Then we need to figure out the buffer index pointed by the output tensor the layer/op "l"
    auto buffer_input_index = model_clone->subgraphs[0]->tensors[tensor_input_index]->buffer;


    // Using the input tensor index we need to find the actual tensor to insert in the base model.
    // Since every data structure is a unique pointer, we need to move the actual
    // object from the clone model to the base model. This will delete the object in the clone model

    model_base->subgraphs[0]->tensors.insert(model_base->subgraphs[0]->tensors.end(),
                                             std::move(model_clone->subgraphs[0]->tensors[tensor_input_index]));

    // For storing the current shape before padding
    int shape1= model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[1];
    int shape2= model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[2];

    //  Now update the shape of the tensor depending upon the padding used. The tensor added will be the last tensor
    model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[1] = shape1+ left_pad + right_pad;
    model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[2] = shape2 + top_pad + bottom_pad;

    // for checking if the shape is coming correctly
//    int shape1m= model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[1];
//    int shape2m= model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->shape[2];


    // Add the name of the newly added tensor. so that it's not null
    int r= rand()%100;
    string tensor_name = "pad"+ std::to_string(r);
    model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->name = tensor_name;


    // Second add a new empty buffer for the above newly added tensor from the clone model to the base model
    model_base->buffers.insert(model_base->buffers.end(), std::move(model_clone->buffers[buffer_input_index]));

    // Now we need to point the newly added tensor to the above buffer. The newly added buffer would be the last one.
    model_base->subgraphs[0]->tensors[tensor_input_index]->buffer = model_base->buffers.size() - 1;


    // Now we need to add the PADV2 layer/op from the ref model into base model before the 0th layer in the inference subgraph
    // if the base model has a single layer, then add the new operator at the end. if there are multiple operators ...then add the new operators after the layer l
    if(model_base->subgraphs[0]->operators.size()==1){
        model_base->subgraphs[0]->operators.insert(model_base->subgraphs[0]->operators.begin(),
                                                   std::move(model_ref->subgraphs[0]->operators[0])); // was end prev
    }
    else{
        model_base->subgraphs[0]->operators.insert(model_base->subgraphs[0]->operators.begin()+l,
                                                   std::move(model_ref->subgraphs[0]->operators[0]));
    }


    // Since the operator table references the operator_codes as a look up table, we need to add the code for the PAD operator
    model_base->operator_codes.insert(model_base->operator_codes.end(), std::move(model_clone->operator_codes[0]));

    //Change the built in code fpr PADv2 which has BuiltinOperator enum code: 60 in the newly inserted operator code
    model_base->operator_codes[model_base->operator_codes.size() -
                               1]->builtin_code = static_cast<tflite::BuiltinOperator>(60); // Pad operator

    // Now we have to point the newly inserted operator to the newly inserted operator code "index" which would the last one
    model_base->subgraphs[0]->operators[l]->opcode_index = model_base->operator_codes.size() - 1;

    // Add input tensor to the newly created PADV2 operator at l=0 as the input of the previous operator at 0 as the graph input
    // model_base->subgraphs[0]->operators[l + 1]->inputs[0] = model_base->subgraphs[0]->operators[l]->outputs[0];
    model_base->subgraphs[0]->operators[l]->inputs[0] = tensor_input_index;

    // Add output tensor to the newly created PADV2 operator at l=0 as the newly inserted tensor in the base model
    model_base->subgraphs[0]->operators[l]->outputs[0] = model_base->subgraphs[0]->tensors.size() - 1;


    model_base->subgraphs[0]->operators[l + 1]->inputs[0] =  model_base->subgraphs[0]->operators[l]->outputs[0];



    // modify the padding buffer 10 of model_ref according to the padding scheme used
    int i = 0;
    for (auto itr = model_ref->buffers[10]->data.begin(); itr != model_ref->buffers[10]->data.end(); itr = itr + 4) {
        if (i == 2) {
            *itr = top_pad;
        }
        if (i == 3) {
            *itr = bottom_pad;

        }
        if (i == 4) {
            *itr = left_pad;
        }
        if (i == 5) {
            *itr = right_pad;
        }
        if(i==0||i==1||i==6||i==7){
            *itr = 0;
        }
        i++;
    }

    // Add the modified padding buffer to the base model
    model_base->buffers.insert(model_base->buffers.end(), std::move(model_ref->buffers[10]));

    // Add the padding tensor which would point to the newly added buffer
    model_base->subgraphs[0]->tensors.insert(model_base->subgraphs[0]->tensors.end(),
                                             std::move(model_ref->subgraphs[0]->tensors[9]));

    // Provide name to the newly added tensor
    // model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->name = tensor_name;

    // point the newly added tensor's buffer  to the newly added buffer. Both buffer and tensors would be the last elements
    model_base->subgraphs[0]->tensors[model_base->subgraphs[0]->tensors.size() - 1]->buffer = model_base->buffers.size() - 1;

    // Add the newly added tensor as an input to the PAD operator
    model_base->subgraphs[0]->operators[l]->inputs[1] = model_base->subgraphs[0]->tensors.size() - 1;


    free(buf);

    free(buf1);





}







// This function inputs the tflite model i.e. a unique_ptr<tflite::ModelT>
// This function extracts the input dimensions: inx, iny, the filter dimensions: kx, ky, Stride: S, Padding: 0(SAME) & 1(VALID), output dimensions: outx, outy  and the layer l of the specified model from a convolutional layer in mobilenet inference graph
void  extract_layer_params(shared_ptr<tflite::ModelT> model_base_extract, int *inx, int *iny, int *kx, int *ky, int *S, int *pad, int *outx, int *outy, int l){


    if(l<0 || l> (model_base_extract->subgraphs[0]->operators.size()-1)){
        printf("Error: Provided layer index out of bounds!\n");
        printf("It should be between 0 and %d\n",(model_base_extract->subgraphs[0]->operators.size()-1));
    }

    // Find the input dimensions
    *inx= model_base_extract->subgraphs[0]->tensors[ model_base_extract->subgraphs[0]->operators[l]->inputs[0]]->shape[1];
    *iny= model_base_extract->subgraphs[0]->tensors[ model_base_extract->subgraphs[0]->operators[l]->inputs[0]]->shape[2];

    // Find the kernel dimensions
    *kx= model_base_extract->subgraphs[0]->tensors[ model_base_extract->subgraphs[0]->operators[l]->inputs[1]]->shape[1];
    *ky= model_base_extract->subgraphs[0]->tensors[ model_base_extract->subgraphs[0]->operators[l]->inputs[1]]->shape[2];

    // Find the output dimensions
    *outx= model_base_extract->subgraphs[0]->tensors[ model_base_extract->subgraphs[0]->operators[l]->outputs[0]]->shape[1];
    *outy= model_base_extract->subgraphs[0]->tensors[ model_base_extract->subgraphs[0]->operators[l]->outputs[0]]->shape[2];


    // Find the stride 'S'

    auto opocode_index_current= model_base_extract->subgraphs[0]->operators[l]->opcode_index;
    auto code= model_base_extract->operator_codes[opocode_index_current]->builtin_code;

    // Variables to store the stride height and width
    auto stride_height=0;
    auto stride_width=0;

    //This code is specific to mobilenet. It has only two types of conv layers: CONV2D and DEPTHWISE CONV. The stride is either 1,1 or 2,2
    // Get the stride parameters for both CONV2D and DEPTHWISE CONV
    if(code==3){ // if CONV 2D
        stride_height=model_base_extract->subgraphs[0]->operators[l]->builtin_options.AsConv2DOptions()->stride_h;
        stride_width= model_base_extract->subgraphs[0]->operators[l]->builtin_options.AsConv2DOptions()->stride_w;
    }
    else{ // if DEPTHWISE CONV
        stride_height=model_base_extract->subgraphs[0]->operators[l]->builtin_options.AsDepthwiseConv2DOptions()->stride_h;
        stride_width= model_base_extract->subgraphs[0]->operators[l]->builtin_options.AsDepthwiseConv2DOptions()->stride_w;
    }
    *S=stride_height;

    *pad=0; // Padding is always same by default



}



// Note: This code is specific to mobilenet

// Since there are only three different types of convolution layers in mobilenet as:

// CASE1-  Input: (2x,2x), kernel: (3,3), stride:2, padding: "SAME", Output: (x,x). This is equivalent to: Input: (2x+1,2x+1) [right and bottom padded by zero point], kernel: (3,3), stride=2, padding: "VALID", Output: (x,x)
// CASE2- Input: (x,x), kernel: (3,3), stride:1,  padding: "SAME", Output: (x,x). This is equivalent to: Input: (x+2,x+2) [padded right, bottom, top and left padded by zero point], kernel: (3,3), stride:1, padding: "VALID", Output: (x,x)
// CASE3- Input: (x,x), kernel: (1,1), stride:1, padding: "SAME", Output: (x,x). This is equivalent to: Input: (x,x) [No pad], kernel: (1,1), stride=1, padding: "VALID", Output: (x,x)

// Function code: This function would take the input dimensions (would extracted by analyzing the flatbuffer layer/op): (inx, iny), kernel size: (kx, ky), stride: S, pad= 0(SAME), 1(VALID), output dimension: (outx, outy), output pixel/slice to track: (outx1, outy1, outx2, outy2 )
// and returns: corresponding input slice: (inx1, iny1, inx2, iny2), padding parameters: (top_pad, bottom_pad,left_pad, right_pad) needed and the  corresponding input tensor dimensions: (Tx, Ty)

void track_cross_fuse(int inx, int iny, int kx, int ky, int S, int pad, int outx, int outy, int outx1, int outy1, int outx2, int outy2, int *inx1, int *iny1, int *inx2, int *iny2,int *top_pad, int *bottom_pad,int *left_pad, int *right_pad, int *Tx, int *Ty ){

    // Convert the tensor dataflow to "VALID" padding
    // let all the default padding be zero to begin with
    *left_pad=0;
    *right_pad=0;
    *top_pad=0;
    *bottom_pad=0;

    // if CASE1
    if(outx==inx/2 && outy==iny/2 && S==2 && pad==0 && kx==3 && ky==3){
        inx++;
        iny++;
        pad=1; // VALID PAD

        // Calculate the corresponding input pixel point or slice from the output pixel point or slice: int outx1, int outy1, int outx2, int outy2. * Using the formula for convolution from the paper
        *inx1 = max_int(outx1*S,0) ;
        *iny1=max_int(outy1*S,0);
        *inx2= min_int(outx2*S+kx-1,inx-1);
        *iny2=min_int(outy2*S+ky-1,iny-1);

        // Detect the required padding: Right and bottom
        if(*iny2<iny-1){ // not in padding zone
            *right_pad=0;
        }
        else{
            *right_pad=1; // in right padding zone
        }

        if(*inx2<inx-1){ // not in padding zone
            *bottom_pad=0;
        }
        else{
            *bottom_pad=1; // in bottom padding zone
        }

        // The above extracts all the input region including any padding points (since padding is used as a separate operator before this current layer and after the previous layer)
        // and so we need to exclude the padding points and find only the actual output pixels to correctly backtrack the previous layer
        // The above output with padding pixels goes from (inx1, iny1) to (inx2, iny2)...but we need to exclude the padding pixels depending upon the detected padding scheme
        // so if left_pad=1, exclude all the points/pixels "i" ( iny1<=i<=iny2) where i==0
        // so if right_pad=1, exclude all the points/pixels "j" (iny1<=j<=iny2) where j==iny-1
        // so if top_pad=1, exclude all the points/pixels "k" ( inx1<=k<=inx2) where k==0
        // so if bottom_pad=1, exclude all the points/pixels "l" ( inx1<=l<=inx2) where l==inx-1
        if(*left_pad==1){
            *iny1=*iny1+1;
        }
        if(*right_pad==1){
            *iny2= *iny2-1;
        }
        if(*top_pad==1){
            *inx1=*inx1+1;
        }
        if(*bottom_pad==1){
            *inx2=*inx2-1;
        }

// No need to subtract (shift origin) for this scenario as the left and top padding not present.

//        // Now finally subtract "1" from all the corner pixels to adjust the origin. That too only when either of the padding is '1'
//        if(*left_pad==1 || *right_pad==1 || *top_pad==1||*bottom_pad==1){
//            *inx1=*inx1-1;
//            *iny1=*iny1-1;
//            *inx2=*inx2-1;
//            *iny2=*iny2-1;
//        }

//
//        *right_pad=0;
//        *left_pad=0;
//        *top_pad=0;
//        *bottom_pad=0;

        // Detect the intermediate tensor size. Please note now this will be after applying the padding operator. So output of padding operator needs to increased acc.
        *Tx=*inx2-*inx1+1;
        *Ty=*iny2-*iny1+1;


    }


    //CASE2
    if(outx==inx && outy==iny && S==1 && pad==0 && kx==3 && ky==3){
        inx= inx + kx-1;
        iny= iny+ ky-1;
        pad=1; // VALID PAD

        // Calculate the corresponding input pixel point or slice from the output pixel point or slice: int outx1, int outy1, int outx2, int outy2. * Using the formula for convolution from the paper
        *inx1 = max_int(outx1*S,0) ;
        *iny1=max_int(outy1*S,0);
        *inx2= min_int(outx2*S+kx-1,inx-1);
        *iny2=min_int(outy2*S+ky-1,iny-1);

        // Detect the required padding: top, bottom, left and right
        if(*inx1>0){ // not in padding zone
            *top_pad=0;
        }
        else{
            *top_pad=1; // in top padding zone
        }

        if(*inx2<inx-1){ // not in padding zone
            *bottom_pad=0;
        }
        else{
            *bottom_pad=1; // in bottom padding zone
        }

        if(*iny1>0){ // not in padding zone
            *left_pad=0;
        }
        else{
            *left_pad=1; // in bottom padding zone
        }
        if(*iny2<iny-1){ // not in padding zone
            *right_pad=0;
        }
        else{
            *right_pad=1; // in bottom padding zone
        }

        // The above extracts all the input region including any padding points (since padding is used as a separate operator before this current layer and after the previous layer)
        // and so we need to exclude the padding points and find only the actual output pixels to correctly backtrack the previous layer
        // The above output with padding pixels goes from (inx1, iny1) to (inx2, iny2)...but we need to exclude the padding pixels depending upon the detected padding scheme
        // so if left_pad=1, exclude all the points/pixels "i" ( iny1<=i<=iny2) where i==0
        // so if right_pad=1, exclude all the points/pixels "j" (iny1<=j<=iny2) where j==iny-1
        // so if top_pad=1, exclude all the points/pixels "k" ( inx1<=k<=inx2) where k==0
        // so if bottom_pad=1, exclude all the points/pixels "l" ( inx1<=l<=inx2) where l==inx-1
        if(*left_pad==1){
            *iny1=*iny1+1;
        }
        if(*right_pad==1){
            *iny2= *iny2-1;
        }
        if(*top_pad==1){
            *inx1=*inx1+1;
        }
        if(*bottom_pad==1){
            *inx2=*inx2-1;
        }

//        // Now finally subtract "1" from all the corner pixels to adjust the origin. That too only when either of the padding is '1'
//        if(*left_pad==1 || *right_pad==1 || *top_pad==1||*bottom_pad==1){
//            *inx1=*inx1-1;
//            *iny1=*iny1-1;
//            *inx2=*inx2-1;
//            *iny2=*iny2-1;
//        }


        // Now finally subtract "1" from all the corner pixels to adjust the origin.
        // This must be done always because due to symmetric padding on all sides i.e. left, top, bottom and right...we must subtract '1' from all coordinates...
        // irrespective if the region is inside padding or not

        *inx1=*inx1-1;
        *iny1=*iny1-1;
        *inx2=*inx2-1;
        *iny2=*iny2-1;




        // Detect the intermediate tensor size. Please note now this will be after applying the padding operator. So output of padding operator needs to increased acc.
        *Tx=*inx2-*inx1+1;
        *Ty=*iny2-*iny1+1;

    }

    // CASE3
    if(outx==inx && outy==iny && S==1 && pad==0 && kx==1 && ky==1){
        pad=1; // VALID PAD
        // No padding required as 1*1 pointwise conv

        // Calculate the corresponding input pixel point or slice from the output pixel point or slice: int outx1, int outy1, int outx2, int outy2. * Using the formula for convolution from the paper
        *inx1 = max_int(outx1*S,0) ;
        *iny1=max_int(outy1*S,0);
        *inx2= min_int(outx2*S+kx-1,inx-1);
        *iny2=min_int(outy2*S+ky-1,iny-1);

        // No pad. Input slice same as output
        *left_pad=0;
        *right_pad=0;
        *top_pad=0;
        *bottom_pad=0;

        // Detect the intermediate tensor size
        *Tx=*inx2-*inx1+1;
        *Ty=*iny2-*iny1+1;


    }


    // Print the output for testing

    // padding needed
//    printf("Padding needed\n:");
//    printf("Top pad: %d\n",top_pad);
//    printf("Bottom pad: %d\n",bottom_pad);
//    printf("Left pad: %d\n",left_pad);
//    printf("Right pad: %d\n",right_pad);
//    printf("\n");
//    printf("\n");
//    printf("\n");
//
//    // Sizes of the intermediate tensor
//    printf("Sizes of the intermediate tensor\n");
//    printf("%d\n",Tx);
//    printf("%d\n", Ty);
//
//    printf("\n");
//    printf("\n");
//    printf("\n");
//
//    // input slice required
//
//    printf("Input slice required\n");
//    printf("%d\n",inx1);
//    printf("%d\n", iny1);
//    printf("%d\n",inx2);
//    printf("%d\n", iny2);
//
//    printf("\n");
//    printf("\n");
//    printf("\n");
//
//


}




// NOTE*** make sure the model you want to partition is renamed to "model_baseline_cross_fusion.tflite" and "model_split" in the current directory
// To get the mutable model use all the mutable API's
int crossfusegen_main(int p1, int q1, int p2, int q2) { // takes the final output coordinates of the cross fused partitions to generate



    FILE *f1 = fopen("model_baseline_cross_fusion.tflite", "rb");
//    FILE *f1 = fopen(filename.c_str(), "rb"); // here provide the correct base model to partition
    fseek(f1, 0, SEEK_END);
    long f1size = ftell(f1);
    fseek(f1, 0, SEEK_SET);
    char *buf1 = (char *) malloc(f1size);
    fread(buf1, f1size, 1, f1);
    fclose(f1);

    // Create the resolver and hasher to unpack the models into variables


    auto resolver = flatbuffers::resolver_function_t( // Get the resolver used to unpack the buffer into another model for modifying it
            [](void **pointer_adr, flatbuffers::hash_value_t hash) {
                (void) pointer_adr;
                (void) hash;
                // Don't actually do anything, leave variable null.
            });


    auto rehasher = flatbuffers::rehasher_function_t( // Get the rehasher used to create a flatbuffer latter
            [](void *pointer) -> flatbuffers::hash_value_t {
                (void) pointer;
                return 0;
            });


    auto model1 = tflite::UnPackModel(buf1, &resolver);

    auto model2 = tflite::UnPackModel(buf1, &resolver);

    // convert the unique pointer to a shared pointer
    std::shared_ptr<tflite::ModelT> model_base_extract = std::move(model1); // for reference

    std::shared_ptr<tflite::ModelT> model_base_mutate = std::move(model2); // for mutation


    // Variables to store the extract_layer_params
    int inx, iny, kx, ky, S, pad, outx, outy;

    // Variables to store the cross fusion track params
    int inx1, iny1, inx2, iny2, outx1, outy1, outx2, outy2, top_pad, bottom_pad, left_pad, right_pad, Tx, Ty;


    for (int i = (model_base_extract->subgraphs[0]->operators.size()-1); i >= 0; i--) { // start from the last layer and then gradually move to the first layer
        extract_layer_params(model_base_extract, &inx, &iny, &kx, &ky, &S, &pad, &outx, &outy, i);




        if (i == (model_base_extract->subgraphs[0]->operators.size() - 1)) { // last layer
            // To generate pyramid for the (p,q) final output pixel.
            outx1 = p1;
            outy1 = q1;
            outx2 = p2;
            outy2 = q2;
            track_cross_fuse(inx, iny, kx, ky, S, pad, outx, outy, outx1, outy1, outx2, outy2, &inx1, &iny1, &inx2,
                             &iny2, &top_pad, &bottom_pad, &left_pad, &right_pad, &Tx, &Ty);

        } else {// if not the last layer, here we need to use the input of the previous layer as our expected output
            outx1 = inx1;
            outy1 = iny1;
            outx2 = inx2;
            outy2 = iny2;
            track_cross_fuse(inx, iny, kx, ky, S, pad, outx, outy, outx1, outy1, outx2, outy2, &inx1, &iny1, &inx2,
                             &iny2, &top_pad, &bottom_pad, &left_pad, &right_pad, &Tx, &Ty);


        }


//        printf("For the layer: %d\n", i);
//        printf("\n THE TRACKED PARAMS ARE: \n");
//
//        printf("inx1 is: %d\n", inx1);
//        printf("iny1 is: %d\n", iny1);
//        printf("inx2 is: %d\n", inx2);
//        printf("iny2 is: %d\n", iny2);
//
//        printf("outx1 is: %d\n", outx1);
//        printf("outy1 is: %d\n", outy1);
//        printf("outx2 is: %d\n", outx2);
//        printf("outy2 is: %d\n", outy2);
//
//        printf("top_pad is: %d\n", top_pad);
//        printf("bottom_pad is: %d\n", bottom_pad);
//        printf("left_pad is: %d\n", left_pad);
//        printf("right_pad is: %d\n", right_pad);
//
//        printf("Tx is: %d\n", Tx);
//        printf("Ty is: %d\n", Ty);
//        printf("\n");
//        printf("\n");
//        printf("\n");

        // copy padding variables
        int t = top_pad;
        int b = bottom_pad;
        int l = left_pad;
        int r = right_pad;


        // update the mutable model...i.e. intermediate tensors taking the last tensor as 1.1
        auto opocode_index_current = model_base_mutate->subgraphs[0]->operators[i]->opcode_index;
        auto code = model_base_mutate->operator_codes[opocode_index_current]->builtin_code;

        auto tensor_weights = model_base_mutate->subgraphs[0]->operators[i]->inputs[1]; // tensor number of input tensor
        auto kernel_height = model_base_mutate->subgraphs[0]->tensors[tensor_weights]->shape[1];
        auto kernel_width = model_base_mutate->subgraphs[0]->tensors[tensor_weights]->shape[2];
        auto tensor_output = model_base_mutate->subgraphs[0]->operators[i]->outputs[0];



        if (i == model_base_mutate->subgraphs[0]->operators.size() - 1) { // if it is the last operator
            for (int i = 0; i < model_base_mutate->subgraphs[0]->tensors[tensor_output].get()->shape.size(); i++) {
                if (i == 1) // modify only width
                    model_base_mutate->subgraphs[0]->tensors[tensor_output]->shape[i] = p2-p1+1;
                if (i == 2) // modify only height
                    model_base_mutate->subgraphs[0]->tensors[tensor_output]->shape[i] = q2-q1+1;
            }
        }

        auto shape_output_height = model_base_mutate->subgraphs[0]->tensors[tensor_output]->shape[1];
        auto shape_output_width = model_base_mutate->subgraphs[0]->tensors[tensor_output]->shape[2];
        // Variables to store the stride height and width
        auto stride_height = 0;
        auto stride_width = 0;
        // Get the stride parameters for both CONV2D and DEPTHWISE CONV
        if (code == 3) { // if CONV 2D
            stride_height = model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsConv2DOptions()->stride_h;
            stride_width = model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsConv2DOptions()->stride_w;
        } else { // if DEPTHWISE CONV
            stride_height = model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsDepthwiseConv2DOptions()->stride_h;
            stride_width = model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsDepthwiseConv2DOptions()->stride_w;
        }

        auto shape_input_height = 0;
        auto shape_input_width = 0;

        if (stride_height == 2 && stride_width == 2) {
            if (i != 0) { // not first layer
                shape_input_height = (shape_output_height) * stride_height + 1;
                shape_input_width = (shape_output_width) * stride_width + 1;

//                shape_input_height = Tx;
//                shape_input_width = Ty;


                // change padding to valid
                if (code == 3)
                    model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsConv2DOptions()->padding = static_cast<tflite::Padding>(1);
                else
                    model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsDepthwiseConv2DOptions()->padding = static_cast<tflite::Padding>(1);
            }
        }

        if (stride_height == 1 && stride_width == 1) {
            shape_input_height = shape_output_height + kernel_height - 1;
            shape_input_width = shape_output_width + kernel_width - 1;
            // change padding to valid
            if (code == 3)
                model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsConv2DOptions()->padding = static_cast<tflite::Padding>(1);
            else
                model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsDepthwiseConv2DOptions()->padding = static_cast<tflite::Padding>(1);


        }

        if (i == 0) { // if first layer
            shape_input_height = (shape_output_height) * stride_height + 1;
            shape_input_width = (shape_output_width) * stride_width + 1;

            if (code == 3)
                model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsConv2DOptions()->padding = static_cast<tflite::Padding>(1);
            else
                model_base_mutate->subgraphs[0]->operators[i]->builtin_options.AsDepthwiseConv2DOptions()->padding = static_cast<tflite::Padding>(1);

        }

        auto tensor_input = model_base_mutate->subgraphs[0]->operators[i]->inputs[0];

        // Modify shape of input tensor
        for (int i = 0; i < model_base_mutate->subgraphs[0]->tensors[tensor_input].get()->shape.size(); i++) {
            if (i == 1)
                model_base_mutate->subgraphs[0]->tensors[tensor_input]->shape[i] = shape_input_height;
            if (i == 2)
                model_base_mutate->subgraphs[0]->tensors[tensor_input]->shape[i] = shape_input_width;
        }

        // ADD pading op only if non zero padding parameters are present
        if (l == 0 && r == 0 && t == 0 && b == 0) {}
        else{
            if (i == 0) { // it is the first layer
                //Add_PADV2(model_base_mutate,0,l,r,t,b);
                // Then we have to add the PAD operator at the begining i.e. before the input... so we have to design such a pad operator function

                Add_PADV2_top(model_base_mutate, i, l, r, t, b);
                // subtract the padding from the input tensor of op/layer 0.. see if modifications are needed in the Add_PADV2_top code itself
                auto out_1 = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->inputs[0]]->shape[1];
                auto out_2 = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->inputs[0]]->shape[2];

                model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->inputs[0]]->shape[1] = out_1 - (top_pad + bottom_pad);
                model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->inputs[0]]->shape[2]=  out_2 - (left_pad + right_pad);

                // change the output tensor of padding operator acc
                model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->outputs[0]]->shape[1] = out_1;
                model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->outputs[0]]->shape[2]=  out_2;





            } else { // if it is not the first layer
                Add_PADV2(model_base_mutate, i - 1, l, r, t, b);

                // subtract the padding from output tensors of layer i-1
                auto out_1 = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i -
                                                                                                                 1]->outputs[0]]->shape[1];
                auto out_2 = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i -
                                                                                                                 1]->outputs[0]]->shape[2];

                model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i -
                                                                                                    1]->outputs[0]]->shape[1] =
                        out_1 - (top_pad + bottom_pad);
                model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i -
                                                                                                    1]->outputs[0]]->shape[2] =
                        out_2 - (left_pad + right_pad);

                // Also change the output shape of the output tensors of the padding operators at i. They should be decided according to the previous layer at i-1
//
//            model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->outputs[0]]->shape[1]=out_1-(top_pad+bottom_pad);
//            model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->outputs[0]]->shape[2]=out_2-(left_pad+right_pad);

                model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->outputs[0]]->shape[1] = out_1;
                model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->outputs[0]]->shape[2] = out_2;

            }





        }






        // Get the shape of baseline model
        int base_in_x = model_base_extract->subgraphs[0]->tensors[model_base_extract->subgraphs[0]->operators[i]->inputs[0]]->shape[1];
        int base_out_x = model_base_extract->subgraphs[0]->tensors[model_base_extract->subgraphs[0]->operators[i]->outputs[0]]->shape[1];

        int base_in_y = model_base_extract->subgraphs[0]->tensors[model_base_extract->subgraphs[0]->operators[i]->inputs[0]]->shape[2];
        int base_out_y = model_base_extract->subgraphs[0]->tensors[model_base_extract->subgraphs[0]->operators[i]->outputs[0]]->shape[2];


        if(model_base_extract->subgraphs[0]->operators.size()==1){ // for a single layer. No comparision needed.
            break;
        }

        // Variables to store tensor shape for mutated model
        int mutate_in_x = 0;
        int mutate_out_x = 0;
        int mutate_in_y = 0;
        int mutate_out_y = 0;

        if (l == 0 && r == 0 && t == 0 && b == 0) { // NO PAD operator added
            mutate_in_x = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->inputs[0]]->shape[1];
            mutate_out_x = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->outputs[0]]->shape[1];
            mutate_in_y = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->inputs[0]]->shape[2];
            mutate_out_y = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i]->outputs[0]]->shape[2];

        } else {
            mutate_in_x = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i +
                                                                                                              1]->inputs[0]]->shape[1];
            mutate_out_x = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i +
                                                                                                               1]->outputs[0]]->shape[1];
            mutate_in_y = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i +
                                                                                                              1]->inputs[0]]->shape[2];
            mutate_out_y = model_base_mutate->subgraphs[0]->tensors[model_base_mutate->subgraphs[0]->operators[i +
                                                                                                               1]->outputs[0]]->shape[2];


        }
//

//
        if(base_in_x==mutate_in_x && base_in_y==mutate_in_y && base_out_x==mutate_out_x && base_out_y==mutate_out_y){
            break;
        }







    }



    flatbuffers::FlatBufferBuilder fbb1;
    fbb1.Finish(CreateModel(fbb1, model_base_mutate.get(), &rehasher),
                tflite::ModelIdentifier());
    auto len1 = fbb1.GetSize(); // get the length of the flatbuffer
    printf("size of the modified buffer is: %d\n", len1);
    char *buf_modified = (char *) malloc(
            len1); // Generate a char buff array to store the data using the length of flatbuffers
    memcpy(buf_modified, fbb1.GetBufferPointer(),
           len1); // copying the flatbuffer data into the generated character buffer
    std::ofstream outfile;
    std::string name = "model_pyramid_" + std::to_string(p1) + "_" + std::to_string(q1) + "_"+ std::to_string(p2) + "_" + std::to_string(q2)  + ".tflite";
    outfile.open(name, std::ios::binary | std::ios::out); // save the model in a file
    outfile.write(buf_modified, len1);
    outfile.close();

    // free the dynamically allocated data

    free(buf_modified);

    free(buf1);

    return 0;
    //printf("ALL SUCCESSFULLY...Completed!");


}


