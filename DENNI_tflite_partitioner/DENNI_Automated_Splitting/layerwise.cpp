// Include the necessary files
#include "flatbuffers/flatbuffers.h"
#include "schema_generated.h"
#include <fstream>   // C++ header file for file access
#include <iostream>  // C++ header file for printing
#include <cstdio>
#include "flatbuffers/idl.h"
#include "flatbuffers/registry.h"
#include "flatbuffers/util.h"
#include <windows.h>
using namespace std;

// This function splits a model layerwise
void Layerwise(string filename, int Padv2_consider){ //model_filename is the file to partition, Padv2 flag decides if it is considered a separate layer while splitting or not
    // Read file into a malloced byte array

    //FILE *f = fopen("model.tflite", "rb"); // here provide the correct base model to partition
    FILE *f = fopen(filename.c_str(), "rb"); // here provide the correct base model to partition
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buf = (char *)malloc(fsize);
    fread(buf, fsize, 1, f);
    fclose(f);

    auto model = tflite::GetMutableModel(buf); // to get the mutable model from the buffer

    auto resolver = flatbuffers::resolver_function_t( // Get the resolver used to unpack the buffer into another model for modifying it
            [](void **pointer_adr, flatbuffers::hash_value_t hash) {
                (void)pointer_adr;
                (void)hash;
                // Don't actually do anything, leave variable null.
            });

    auto rehasher = flatbuffers::rehasher_function_t( // Get the rehasher used to create a flatbuffer latter
            [](void *pointer) -> flatbuffers::hash_value_t {
                (void)pointer;
                return 0;
            });

    auto model1 = tflite::UnPackModel(buf, &resolver); // Unpack the buf into another model for analysis



    /*
     find if there are operator code entries for Convolution2D, Depthwise Convolution and fully connected layer in our model.
     To find the above we will iterate through our model for all operator_codes table
     and see if the builtin_code matches the enum entry for BuiltinOperator enum as defined in the schema.fbs
     For e.g. BuiltinOperator enum has a byte value of 3 for CONV_2D, 4 for DEPTHWISE_CONV_2D and 9 for FULLY_CONNECTED
    */

    /*
     * entry_opcodes: Stores the index of the entry for Convolution2D, Depthwise Convolution and fully connected layer in the operator_codes table.
     This is referenced by the operator table in subgraph
     */
    vector<int> entry_opcodes;
    for(int i=0;i<model1->operator_codes.size();i++) {
        auto code= model1->operator_codes[i]->builtin_code; // fetch successively built in code for all operators.
        if(Padv2_consider==1){
            if(code==3||code==4||code==9||code==60) // check if code matches BuiltinOperator enum byte values for Convolution2D, Depthwise Convolution, fully connected layer & PADV2 and avgpool2d (was 1 but was removed to not treat its as a separate layer)
                entry_opcodes.push_back(i);
        }
        else{
            if(code==3||code==4||code==9) // check if code matches BuiltinOperator enum byte values for Convolution2D, Depthwise Convolution, fully connected layer & PADV2 and avgpool2d (was 1 but was removed to not treat its as a separate layer)
                entry_opcodes.push_back(i);
        }

    }



    /*
     * Find the number of Convolution2D, Depthwise Convolution and fully connected layers in the model and there operator index
    */
    /* We can determine this by using the OpcodeIndex of model.subgraph[0].operator table and if that matches with entry_opcodes */


    /*  No_of_layers: stores the number of Convolution2D, Depthwise Convolution and fully connected layers in the network */
    int No_of_layers = 0;

    /* vector "opindx": stores the indexes of opcodes of the operator table of model.subgraph[0] which are Convolution2D, Depthwise Convolution, Avgpool2D, Maxpool2D and fully connected layer*/
    vector<int> opindx;

    /* current_partition: stores the index of current partition being processed*/
    int current_partition=0;

    /* calculate the number of layers (Convolution2D, Depthwise Convolution and fully connected) present in the convolutional Neural Network */
    for(int i=0;i<model1->subgraphs[0]->operators.size();i++) // Goes through all the operators in our subgraph
        for(int j=0;j<entry_opcodes.size();j++) // Goes through all opcodes in entry_opcodes
            if(model1->subgraphs[0]->operators[i]->opcode_index==entry_opcodes[j]) // determine if an operator is a Convolution2D, Depthwise Convolution or a fully connected layer
            {
                No_of_layers++; // increment
//               if(No_of_layers==26||No_of_layers==27) { // for partitioning at layer "i", use "No_of_layers==i||No_of_layers==i+1". e.g 2 layer will be 2,3
//                   printf("i is: %d\n", i);
                opindx.push_back(i); // adds the index of the operator
                //         }
//

                //printf("No of layers: %d \n",No_of_layers);
//            if(No_of_layers==24) // Added code to split the graph into two parts depending upon the layer number chosen

            }

    /* Now using the operator index in opindx_fc partition the model layerwise and determine which operators to include with it in the partition
     logic is to include the operators traversing both upwards and downwards from the opindx_fc which are not Convolution2D, Depthwise Convolution, Avgpool2D, Maxpool2D or a fully connected*/

    for(auto itr=opindx.begin();itr!=opindx.end();itr++) // iterate through the opindx vector
    {
        /* ops_include: store operators to include with the current opindx vector.
         *logic is to include all the operators from the begining till the current opindx index
           and then till the next opindx */
        vector<int> ops_include;

        if(itr==opindx.begin()) { // if it is the index of first opindx operator include all the operators from 0 till this index
            for (int i = 0; i < *itr; i++)
                ops_include.push_back(
                        i); // include all the operators from the begining till the current opindx index
        } else{
            for(int i=*itr;i>*(itr-1) && (i!=*itr) ; i--) // if it is not the first element than include operator indexes between current and previous (not including current and previous)
                ops_include.push_back(i);
        }

        if(itr!=(opindx.end()-1)) // if the current opindx is not the last opindx operator. Include itself and all the operators before the next
        {
            for(int i=*itr;i<*(itr+1);i++)
                ops_include.push_back(i);// include all the operators till the next opindx
        } else{// if last element include from current opindx till the last operators
            for(int i=*itr;i<model1->subgraphs[0]->operators.size();i++)
                ops_include.push_back(i);

        }


        /*
         * Figure out the indexes of all used tensors for the current partition*/
        /*vector "tensors_used" stores the indexes of all the tensors used for the current partition*/
        vector <int> tensors_used;
        for(auto ptr= ops_include.begin();ptr!=ops_include.end();ptr++) // iterate through all the operators in ops_include
        {
            // Add all the input tensors for all operators in the partition
            for(int i=0;i<model1->subgraphs[0]->operators[*ptr]->inputs.size();i++)
                tensors_used.push_back(model1->subgraphs[0]->operators[*ptr]->inputs[i]);

            // Add all the output tensors for all operators in the partition
            for(int i=0;i<model1->subgraphs[0]->operators[*ptr]->outputs.size();i++)
                if(ptr==ops_include.end()-1) // Add output only for the last operator in a graph. To avoid duplicate input/output tensors
                    tensors_used.push_back(model1->subgraphs[0]->operators[*ptr]->outputs[i]);


        }


        /*Figure out the indexes of all buffers used in the current partition. By referencing it from the tensor indexes*/
        vector <int> buffers_used;

        for(auto ptr_tensors= tensors_used.begin();ptr_tensors!=tensors_used.end();ptr_tensors++) // Using the tensor index in the partition find the buffers used.
            buffers_used.push_back(model1->subgraphs[0]->tensors[*ptr_tensors]->buffer);

        /* Figure out the indexes of all buffers not used in the current partition and which are to be deleted. Using the data from buffers to be deleted */
        vector <int> buffers_not_used; // stores the buffers to be deleted from unpartitioned model for current partition
        for(int i=0;i<model1->buffers.size();i++)
            if(find(buffers_used.begin(),buffers_used.end(),i)!=buffers_used.end()){ //true means item present. False otherwise
                // Buffer is used. So not be added to the buffer_not_used vector
            }
            else{
                // buffer to be added to the buffer_not_used vector and is deleted
                buffers_not_used.push_back(i);

            }

        /* figure out the indexes of all the tensors not used in the partition */
        vector<int> tensors_not_used; // stores the tensors not used in the partition
        for(int i=0;i<model1->subgraphs[0]->tensors.size();i++)
            if(find(tensors_used.begin(),tensors_used.end(),i)!=tensors_used.end()){ //true means item present. False otherwise
                // Buffer is used. So not be added to the buffer_not_used vector
            }
            else{
                // buffer to be added to the buffer_not_used vector and is deleted
                tensors_not_used.push_back(i);

            }

        //Get a new model clone to modify
        auto model_modify = tflite::UnPackModel(buf, &resolver); // Unpack the buf into another model for modification

        // Delete the buffers not used in the partition using the information about the buffers used
        for(auto ptr_buf_not_used=buffers_not_used.begin();ptr_buf_not_used!=buffers_not_used.end();ptr_buf_not_used++)
            model_modify->buffers[*ptr_buf_not_used]->data.clear();


        /* Delete the tensors not used in the partition using the information from tensors used.
           Also making sure that the tensor vector gets modified when we delete it.
           E.g.  deleting vector at index 2 modifies the tensor vector by placing the vector at index 3 to index 2 and
           so we have to keep track of the previous deletions when deleting current elements */

        /* vector "prev_tensors_delete": keeps track of previous deletions. Also we have to sort the tensors_not_used vector for deletion */
        int prev_tensors_delete=0;
        sort(tensors_not_used.begin(),tensors_not_used.end()); // this sorts out the tensors_not_used vector for correct deletion

        // tensors deletion code
        for(auto ptr_tensors_not_used=tensors_not_used.begin();ptr_tensors_not_used!=tensors_not_used.end();ptr_tensors_not_used++) // use the sorted tensor_used vector for deletion
        {
            model_modify->subgraphs[0]->tensors.erase(model_modify->subgraphs[0]->tensors.begin()+(*(ptr_tensors_not_used)-prev_tensors_delete));// delete the tensors taking into account the past deletions
            prev_tensors_delete++; // store the number of deletions
        }


        /*
         * Delete the unused operators in the partition.
         Similar to tensors first figure out the unused operators and then delete one by one by taking previous deletions into account */

        // First figure out the indexes of all the operators not used in the partition
        // vector "operators_not_used": stores the tensors not used in the partition
        vector<int> operators_not_used;
        for(int i=0;i<model1->subgraphs[0]->operators.size();i++)
            if(find(ops_include.begin(),ops_include.end(),i)!=ops_include.end()){ //true means item present. False otherwise
                // Buffer is used. So not be added to the buffer_not_used vector
            }
            else{
                // buffer to be added to the buffer_not_used vector and is deleted
                operators_not_used.push_back(i);

            }

        /* Delete the operators not used in the partition using the information from ops used.
           Also making sure that the operator vector gets modified when we delete it.
           E.g.  deleting vector at index 2 modifies the operator vector by placing the vector at index 3 to index 2 and
           so we have to keep track of the previous deletions when deleting current elements */

        // vector "prev_ops_delete" keeps track of previous deletions. Also we have to sort the tensors_not_used vector for deletion
        int prev_ops_delete=0;
        sort(operators_not_used.begin(),operators_not_used.end()); // this sorts out the operators_not_used vector for correct deletion

        // operators deletion code
        for(auto ptr_operators_not_used=operators_not_used.begin();ptr_operators_not_used!=operators_not_used.end();ptr_operators_not_used++) // use the sorted tensor_used vector for deletion
        {
            model_modify->subgraphs[0]->operators.erase(model_modify->subgraphs[0]->operators.begin()+(*(ptr_operators_not_used)-prev_ops_delete));// delete the tensors taking into account the past deletions
            prev_ops_delete++; // store the number of deletions
        }


        /* Since both the operator and tensors vector have changed due to modification.
           Assign again the input-output tensors to vector accordingly
           Using model1 and the ops_include split map the inputs and output to the partitioned network
           Tensors Name seems to be the only key that could be used to identify correct tensors for each operation for the partitioned model */

        int model_modify_ops=0; // points to the index of current ops of model_modify
        // for each op used in the partition
        int model_modify_input=0; // points to the index of input of the current ops of model_modify
        int model_modify_output=0; // points to the index of output of the current ops of model_modify

        for(auto itr_ops_inc = ops_include.begin();itr_ops_inc!=ops_include.end();itr_ops_inc++)
        {

            // Go through all inputs and update the tensor indexes of model_modify

            model_modify_input=0; // reset for each new input
            for(auto itr= model1->subgraphs[0]->operators[*itr_ops_inc]->inputs.begin();itr!=model1->subgraphs[0]->operators[*itr_ops_inc]->inputs.end();itr++)
            {

                // *itr stores the index of the tensors in tensors[]
                // using the index in *itr get the name of tensor in tensors[]
                auto name1 = model1->subgraphs[0]->tensors[*itr]->name;

                // using the name find the index of tensor in model_modify (the model clone where we have deleted unused tensors and hence tensor indexes have changed and we have to map the correct index)
                for(int i=0;i<model_modify->subgraphs[0]->tensors.size();i++)
                    if(model_modify->subgraphs[0]->tensors[i]->name.compare(name1)==0){ // if tensor name matches
                        model_modify->subgraphs[0]->operators[model_modify_ops]->inputs[model_modify_input++]=i; // update the input tensor index which the model_modify ops points too
                    }


            }

            // Similarly for outputs of each op

            model_modify_output=0; // reset for each new output
            for(auto itr= model1->subgraphs[0]->operators[*itr_ops_inc]->outputs.begin();itr!=model1->subgraphs[0]->operators[*itr_ops_inc]->outputs.end();itr++)
            {

                // *itr stores the index of the tensors in tensors[]
                // using the index in *itr get the name of tensor in tensors[]
                auto name2 = model1->subgraphs[0]->tensors[*itr]->name;

                // using the name find the index of tensor in model_modify (the model clone where we have deleted unused tensors and hence tensor indexes have changed and we have to map the correct index)
                for(int i=0;i<model_modify->subgraphs[0]->tensors.size();i++)
                    if(model_modify->subgraphs[0]->tensors[i]->name.compare(name2)==0){ // if tensor name matches
                        model_modify->subgraphs[0]->operators[model_modify_ops]->outputs[model_modify_output++]=i; // update the input tensor index which the model_modify points too

                    }

            }
            model_modify_ops++; // increment for next ops to modify in model_modify
        }

        // Now finally assign the input and outputs to the model.
        // Input will be the 0th input of the first ops of the model
        auto model_input = model_modify->subgraphs[0]->operators[0]->inputs[0]; // input to the model
        *model_modify->subgraphs[0]->inputs.data() = model_input;

        // Output will be the 0th output of the last ops of the model
        auto model_output = model_modify->subgraphs[0]->operators[model_modify->subgraphs[0]->operators.size()-1]->outputs[0]; // output to the model
        *model_modify->subgraphs[0]->outputs.data() = model_output;

        // Save the model partitions
        // Re-serialize the data.
        flatbuffers::FlatBufferBuilder fbb1;
        fbb1.Finish(CreateModel(fbb1, model_modify.get(), &rehasher),
                    tflite::ModelIdentifier());
        auto len1 = fbb1.GetSize(); // get the length of the flatbuffer
        printf("size of the modified buffer is: %d\n",len1);
        char *buf_modified = (char *)malloc(len1); // Generate a char buff array to store the data using the length of flatbuffers
        memcpy(buf_modified,fbb1.GetBufferPointer(),len1); // copying the flatbuffer data into the generated character buffer
        std::ofstream outfile;
        std::string name = "model_layerwise_split_layer_"+std::to_string(current_partition+1)+".tflite";
        outfile.open(name, std::ios::binary | std::ios::out); // save the model in a file
        outfile.write(buf_modified,len1);
        outfile.close();

        // free the dynamically allocated data

        free(buf_modified);
        current_partition++; // increments current partition index
        ops_include.clear(); // clear ops_include for next operation
        tensors_used.clear();
        tensors_not_used.clear();
        buffers_used.clear();
        buffers_not_used.clear();
        operators_not_used.clear();

    }


    free(buf);
}

//
// Created by mailr on 3/26/2021.
//

