// Automated Script to perform input and weight splitting cross fusion partition or a single layer of a convolutional Neural Network with Conv2D, DEPTHWISE CONV, AVGPOOL2D and MAXPOOL2D layers
// This scripts reads a text file with the following parameters: i, j, E, F, p, q, w, l, ch_split, ch_base
// i is start index of base chunk, j is the end index of chunk, E, F are height & width of final baseline output, p,q are height & width of expected output (p<E and or F<q => arena splitting),
// w is a redundant parameter which is always 1,  l is the no of layers (l>1 => cross-fusion), ch_split is the output channel array of expected of each of the partition, ch_base output channel array of baseline subgraph
// (any ch_split[i]<= ch_base[i]=> weight channel splitting by computing different output channel across different nodes)

/* Sample e.g. (sample_partition_config_cross_fuse_arena_split.txt) where we want to cross-fuse the layer 1-11 (11 layers of mobilenetv1)
 * with the final baseline output going from 16*16 to 16*1 producing 16 different cross fused arena partitions
we will compile and execute the code in clion IDE. Please make sure name the network_block_name, network_name correctly
string network_block_name="mobilenetv1_block"; (we can change this name to desired folder)
string network_name="mobilenetv1";
During execution the script would pick up layers 1-11 from mobilenetv1/chunk folder and then execute crossfusegen.cpp on it starting from the final output layer going towards the input layer.
This would create a new folder mobilenetv1_block (with 16 folders from Node0 to Node15 each containing different cross-fused arena split subgraph) which can then be further executed on the msp430fr5994 node
*/



/* Sample e.g. (sample_partition_config_weight_split.txt) where we want to split the weights of the first layer of mobilenetv1 by computing first 4 out of 8 channels in one node and then last 4 channels in another
 * string network_block_name="mobilenetv1_block"; (we can change this name to desired folder)
 * During execution the script would pick up layers 1 from mobilenetv1/layerwise (which has already been splitted layerwise using layerwise.cpp) folder and then execute output_split.cpp on it
 * producing two output models in the current directory: layer_1_partition0.tflite and layer_1_partition0.tflite which can then be further executed on the msp430fr5994 node
 * */

// ALL in all, we could directly use the output of MINLP (e.g. like DENNI_sample_MINLP_Output.txt convert it to the desired format) and feed here for partitioning.
// In this way we could excute any arena/weight partitioning oer even arena partitioning followed by weight partitioning (any desired combinations thereof) hich can then be further executed on the msp430fr5994 node!



#include "flatbuffers/flatbuffers.h"
#include "schema_generated.h"
#include <fstream>   // C++ header file for file access
#include <iostream>  // C++ header file for printing
#include <cstdio>
#include "flatbuffers/idl.h"
#include "flatbuffers/registry.h"
#include "flatbuffers/util.h"
#include <filesystem>
#include "crossfusegen.h"
#include "output_split.h"
#include "math.h"


using namespace std;

bool copyFile(const char *SRC, const char* DEST)
{
    std::ifstream src(SRC, std::ios::binary);
    std::ofstream dest(DEST, std::ios::binary);
    dest << src.rdbuf();
    return src && dest;
}

void print_block_read_data(int *i, int*j, int *E, int *F, int *p, int *q, int *l,int  *w,int *ch_split, int *ch_base){

        printf("i is : %d\n",*i);
    printf("j is: %d\n",*j);
    printf("E is :%d\n",*E);
    printf("F is : %d\n",*F);
    printf("p is :%d\n",*p);
    printf("q is :%d\n",*q);
    printf("l is :%d\n",*l);
    printf("w is :%d\n",*w);
    printf("ch_split\n");
    for(int i=0;i<*l;i++)
        printf("%d\n",ch_split[i]);
    printf("ch_base\n");
    for(int i=0;i<*l;i++)
        printf("%d\n",ch_base[i]);
}

int parse_block_data(string filename){
    ifstream file(filename);

    int no_of_partitions=0;
    int x;

    while (file >> x)
        no_of_partitions = x;
    return no_of_partitions;
}


void parse_block_data(string filename, int *i, int*j, int *E, int *F, int *p, int *q, int *l, int *w, int *ch_split, int *ch_base){
// make size at least as large as needed
    const int size = 100;
    float arr[size];

    ifstream file(filename);

    int count = 0;
    float x;

    // check that array is not already full
    // and read integer from file,

    while (count < size && file >> x)
        arr[count++] = x;


    // display the values stored in the array
//    for (int i = 0; i < count; i++)
//        cout << arr[i] <<' ';

    // int *E, int *F, int *p, int *q, int *l, int *ch_split, int *ch_base
    *i=int(arr[0]);
    *j=int(arr[1]);
    *E=int(arr[2]);
    *F=int(arr[3]);
    *p=int(arr[4]);
    *q=int(arr[5]);
    *w=int(arr[6]);
    *l=int(arr[7]);

    for(int i=0;i<int(arr[7]);i++)
        *(ch_split+i)=int(arr[8+i]);
    for(int i=0;i<*l;i++)
        *(ch_base+i)=int(arr[8+int(arr[7])+i]);
}

int check_arena_split(int E, int F, int p, int q){
    if(E==p && F==q)
        return 0;
    else if(E>p || F>q)
        return 1;
    else if(E<p || F<q || E==0 || F==0 ||p==0 ||q==0 ) {
        printf("Error! E and F cannot be less than p and q and none of them could be zero...check block file and its read");
        return -1;
    }
    else{
        return -1;
    }
}




int check_weight_split(int *ch_base, int *ch_split, int l){
    int sum_base_ch=0;
    int sum_split_ch=0;
    for(int i=0;i<l;i++) {
        sum_base_ch+=ch_base[i];
        sum_split_ch+=ch_split[i];
    }

    if(sum_split_ch==sum_base_ch){
        return 0;
    }
    else if(sum_split_ch<sum_base_ch){
        return 1;
    }
    else{
        printf("Error! ...check block file and its read");
        return -1;
    }


}


int calc_arena_coord(int E, int F, int p, int q, int *left1, int *left2, int *right1, int *right2, int* len_hori, int * len_vert){

    // This only works when E%p==0 and F%q==0
    if(E%p==0 && F%q==0){
        *len_hori=F/q;
        *len_vert=E/p;
        for(int i=0;i<F/q;i++){
            for(int j=0;j<E/p;j++){
                left1[i]=i*q;
                left2[j]=j*p;
                right1[i]=((i+1)*(q))-1;
                right2[j]=((j+1)*(p))-1;
            }
        }
        return 0;
    }
    else{
        printf("Error: Eisnot div by p, Fis not div by q, generate partitions by hand\n");
        return -1;
    }


}


void print_arena_coord(int *left1, int *left2, int *right1, int *right2, int* len_hori, int * len_vert){
    for(int i=0;i<*len_hori;i++){
        for(int j=0;j<*len_vert;j++){
            printf("left1: %d, left2: %d, right1: %d, right2: %d \n",left1[i],left2[j],right1[i],right2[j]);
        }
    }
}


namespace fs = std::filesystem;

int i,j,E,F,p,q,l,w; // i is start index of base chunk, j is the end index of chnk, E, F are height & width of final baseline output, p,q are height & width of final baseline output, l is the no of layers
int ch_split[100],ch_base[100]; // ch_split are channels in splitted network and ch_base are channelsin base network.
int len_hori, len_vert;
// To store top and bottom corner coordinates
int left1[100] = {0};
int left2[100] = {0};
int right1[100] = {0};
int right2[100] = {0};
// To get the mutable model use all the mutable API's
int main() {

string network_block_name="mobilenetv1_block2";
string network_name="mobilenetv1";
//string block_name="block1";
// Create a new directory for new experiment
//std::filesystem::create_directory(network_name);

// Read the no of partitions from No_of_Partition.txt file

//string No_of_part_filename="./text12.txt";
//int no_of_partitions= parse_block_data( No_of_part_filename);
//
//int p=no_of_partitions; // ideally p will be in a for loop

//printf("%d\n",no_of_partitions);



string block_filename="./sample_partition_config_cross_fuse_arena_split.txt";

//string block_filename="./sample_partition_config_weight_split.txt";
// Read the block file

    //parse_block_data(block_filename, &i, &j, &E, &F, &p, &q, &l,&w, ch_split, ch_base);

    parse_block_data(block_filename, &i, &j, &E, &F, &p, &q, &l,&w, ch_split, ch_base);
    print_block_read_data(&i, &j, &E, &F, &p, &q, &l,&w, ch_split, ch_base);
int flag_arena_split=check_arena_split(E, F, p, q);
int flag_weight_split=check_weight_split(ch_base, ch_split,l);

if(flag_arena_split==1) {




    //calc_arena_coord(E, F, p, q, &left1,&left2,&right1,&right2,&len_hori,&len_vertl);


    int arena_calc_r = calc_arena_coord(E, F, p, q, left1, left2, right1, right2, &len_hori, &len_vert);
    if (arena_calc_r == -1) {
        printf("issue in arena calculation");
    }

    //print_arena_coord(left1, left2, right1, right2, &len_hori, &len_vert);


}


//    flag_weight_split=1;
//    flag_arena_split=0;
//    i=22;
//    j=26;
    // To test the unsplitted case

    if(flag_arena_split==0 && flag_weight_split==0)// implies no splitting done for the block. Baseline same as the splitted network
    {
        printf("no splitting\n");
        if(i==j){ // single layer

            string fpath1="./"+network_block_name+"/"+"Baseline";
            //string fpath2="./"+network_block_name+"/"+"Node1";
            string fpath2="./"+network_block_name+"/"+"Partition"+"_"+to_string(p)+"/"+"Node1";
            fs::create_directories(fpath1);
            fs::create_directories(fpath2);
            string source_file_path= "./"+network_name+"/"+"layerwise"+"/"+"model_layerwise_split_layer_"+to_string(i)+".tflite";
            string dest_file_path1= fpath1+"/base.tflite";
            string dest_file_path2= fpath2+"/node1.tflite";
            fs::copy(source_file_path,dest_file_path1);
            fs::copy(source_file_path,dest_file_path2);
            //fs::copy("./mobilenetv1/layerwise/model_layerwise_split_layer_1.tflite", "./mobilenetv1_block1/Node1/model.tflite"); // copy file

        }
        else{ // chunk

            string fpath1="./"+network_block_name+"/"+"Baseline";
            //string fpath2="./"+network_block_name+"/"+"Node1";
            string fpath2="./"+network_block_name+"/"+"Partition"+"_"+to_string(p)+"/"+"Node1";
            fs::create_directories(fpath1);
            fs::create_directories(fpath2);
            string source_file_path= "./"+network_name+"/"+"chunk"+"/"+"model_split_layer_"+to_string(i)+"_to_"+to_string(j)+".tflite";
            string dest_file_path1= fpath1+"/base.tflite";
            string dest_file_path2= fpath2+"/node1.tflite";
            fs::copy(source_file_path,dest_file_path1);
            fs::copy(source_file_path,dest_file_path2);

        }

        printf("Block found to be not splitted one! Completed\n");



    }

    // To test arena splitting case


    if(flag_arena_split==1 && flag_weight_split==0) //  implies only arena splitting done for the block
    {
        printf("only arena splitting\n");
        if(i!=j){
            // Create a new folder: Baseline
            string fpath1="./"+network_block_name+"/"+"Baseline";
            fs::create_directories(fpath1);
            string source_file_path= "./"+network_name+"/"+"chunk"+"/"+"model_split_layer_"+to_string(i)+"_to_"+to_string(j)+".tflite";

            // Arena script needs the baseline i to j chunk file to be copied and renamed to "model_baseline_cross_fusion" and "model_split"
            // copy chunk file and rename to model_baseline_cross_fusion
            fs::copy(source_file_path,"./"); // copy to current directory
            string curr_file1="./model_split_layer_"+to_string(i)+"_to_"+to_string(j)+".tflite";
            string rename_file1="./model_baseline_cross_fusion.tflite";
            fs::rename(curr_file1,rename_file1);

            // copy chunk file and rename to model_split
            fs::copy(source_file_path,"./"); // copy to current directory
            string curr_file2="./model_split_layer_"+to_string(i)+"_to_"+to_string(j)+".tflite";
            string rename_file2="./model_split.tflite";
            fs::rename(curr_file2,rename_file2);


            //execute the arena splitting
            int node=0;
            for(int i=0;i<len_hori;i++){
                for(int j=0;j<len_vert;j++){
                    //printf("left1: %d, left2: %d, right1: %d, right2: %d \n",left1[i],left2[j],right1[i],right2[j]);
                    crossfusegen_main(left1[i],left2[j],right1[i],right2[j]);
                    // rename the generated file in current directory and move to a new node folder
                    // create a new node folder
                    string fpath2="./"+network_block_name+"/"+"Node"+to_string(node);
                    fs::create_directories(fpath2);
                    // string crossfuse_gen_file_name="./model_pyramid_"+to_string(left1[i])+"_"+to_string(left2[i])+"_"+to_string(right1[i])+"_"+to_string(right2[i])+".tflite";
//                string path_to_copy=fpath2+"/Node"+to_string(node)+".tflite";

                    //string path_to_copy=fpath2;


                    node++;

                }
            }

            int n=0;
            // copy the generated cross fused blocks to the node folders
            for(int i=0;i<len_hori;i++){
                for(int j=0;j<len_vert;j++){
                    string fpath2="./"+network_block_name+"/"+"Node"+to_string(n)+"/";
                    string crossfuse_gen_file_name="./model_pyramid_"+to_string(left1[i])+"_"+to_string(left2[j])+"_"+to_string(right1[i])+"_"+to_string(right2[j])+".tflite";
                    fs::copy(crossfuse_gen_file_name,fpath2);
                    n++;

                }
            }

            // fs::copy(crossfuse_gen_file_name,fpath2);

            // rename the "model_baseline_cross_fusion" to "base.tflite" and save it in a folder Base
            fs::rename("./model_baseline_cross_fusion.tflite","./base.tflite");
            string source_file_path2="./base.tflite";
            fs::copy(source_file_path2,fpath1);



            // delete the "model_baseline_cross_fusion" and "model_split" files in the current directory
            remove( "model_baseline_cross_fusion.tflite" );
            remove( "model_split.tflite" );

        }
        else if(i==j) // layerwise
        {
            // Create a new folder: Baseline
            string fpath1="./"+network_block_name+"/"+"Baseline";
            fs::create_directories(fpath1);
            string source_file_path= "./"+network_name+"/"+"layerwise"+"/"+"model_layerwise_split_layer_"+to_string(i)+".tflite";

            // Arena script needs the baseline i to j chunk file to be copied and renamed to "model_baseline_cross_fusion" and "model_split"
            // copy chunk file and rename to model_baseline_cross_fusion
            fs::copy(source_file_path,"./"); // copy to current directory
            string curr_file1="./model_layerwise_split_layer_"+to_string(i)+".tflite";
            string rename_file1="./model_baseline_cross_fusion.tflite";
            fs::rename(curr_file1,rename_file1);

            // copy chunk file and rename to model_split
            fs::copy(source_file_path,"./"); // copy to current directory
            string curr_file2="./model_layerwise_split_layer_"+to_string(i)+".tflite";
            string rename_file2="./model_split.tflite";
            fs::rename(curr_file2,rename_file2);


            //execute the arena splitting
            int node=0;
            for(int i=0;i<len_hori;i++){
                for(int j=0;j<len_vert;j++){
                    //printf("left1: %d, left2: %d, right1: %d, right2: %d \n",left1[i],left2[j],right1[i],right2[j]);
                    crossfusegen_main(left1[i],left2[j],right1[i],right2[j]);
                    // rename the generated file in current directory and move to a new node folder
                    // create a new node folder
                    //string fpath2="./"+network_block_name+"/"+"Partition"+"_"+to_string(no_of_partitions)+"/"+"Node1";
                    //string fpath2="./"+network_block_name+"/"+"Node"+to_string(node);
                    string fpath2="./"+network_block_name+"/"+"Partition"+"_"+to_string(p)+"/"+"Node"+to_string(node);
                    fs::create_directories(fpath2);
                    // string crossfuse_gen_file_name="./model_pyramid_"+to_string(left1[i])+"_"+to_string(left2[i])+"_"+to_string(right1[i])+"_"+to_string(right2[i])+".tflite";
//                string path_to_copy=fpath2+"/Node"+to_string(node)+".tflite";

                    //string path_to_copy=fpath2;


                    node++;

                }
            }

            int n=0;
            // copy the generated cross fused blocks to the node folders
            for(int i=0;i<len_hori;i++){
                for(int j=0;j<len_vert;j++){
                    //string fpath2="./"+network_block_name+"/"+"Node"+to_string(n)+"/";
                    string fpath2="./"+network_block_name+"/"+"Partition"+"_"+to_string(p)+"/"+"Node"+to_string(n);
                    string crossfuse_gen_file_name="./model_pyramid_"+to_string(left1[i])+"_"+to_string(left2[j])+"_"+to_string(right1[i])+"_"+to_string(right2[j])+".tflite";
                    fs::copy(crossfuse_gen_file_name,fpath2);
                    n++;

                }
            }

            // fs::copy(crossfuse_gen_file_name,fpath2);

            // rename the "model_baseline_cross_fusion" to "base.tflite" and save it in a folder Base
            fs::rename("./model_baseline_cross_fusion.tflite","./base.tflite");
            string source_file_path2="./base.tflite";
            fs::copy(source_file_path2,fpath1);



            // delete the "model_baseline_cross_fusion" and "model_split" files in the current directory
            remove( "model_baseline_cross_fusion.tflite" );
            remove( "model_split.tflite" );

        }




    }


    if(flag_weight_split==1 && flag_arena_split==0 ) //  implies only weight splitting done for the block
    {
        printf("only weight splitting\n");
        if(i!=j){// multiple layers
        // copy all the models into current directory
            string fpath1="./"+network_block_name+"/"+"Baseline";
            fs::create_directories(fpath1);
            for(int net=i;net<=j;net++){
                string source_file_path= "./"+network_name+"/"+"layerwise"+"/"+"model_layerwise_split_layer_"+to_string(net)+".tflite";

                fs::copy(source_file_path,"./"); // copy to current directory
                fs::copy(source_file_path,fpath1);// copy to baseline folder
            }

            double temp;
            for(int k=0;k<l;k++){
            string name_model="model_layerwise_split_layer_"+to_string(i+k)+".tflite";
            temp=(double)ch_split[k];
            conv_depthwise_conv_output_split(name_model,temp,(i+k));

           }


//            for (int z=0;z<w;z++){// for all the weight nodes
//                string fpath2="./"+network_block_name+"/"+"Node"+to_string(z); // create separate node folders
//                fs::create_directories(fpath2);
//                int t= z+i;
//                string source_file_path2= "./layer_"+to_string(t)+"_partition_"+to_string(z)+".tflite";
//                fs::copy(source_file_path2,fpath2);//copy to the node folder


          //  }

            // move the layerwise model to baseline folders



        }
        else if(i==j){ // single layer

            printf("only weight splitting\n");
            //if(i!=j){// multiple layers
                // copy all the models into current directory
                string fpath1="./"+network_block_name+"/"+"Baseline";
                fs::create_directories(fpath1);
                for(int net=i;net<=j;net++){
                    string source_file_path= "./"+network_name+"/"+"layerwise"+"/"+"model_layerwise_split_layer_"+to_string(net)+".tflite";

                    fs::copy(source_file_path,"./"); // copy to current directory
                    fs::copy(source_file_path,fpath1);// copy to baseline folder
                }

                double temp;
                for(int k=0;k<l;k++){
                    string name_model="model_layerwise_split_layer_"+to_string(i+k)+".tflite";
                    temp=(double)ch_split[k];
                    conv_depthwise_conv_output_split(name_model,temp,(i+k));

                }
        }
        else{
            printf("error\n");
        }


    }

//    if(flag_weight_split==1 && flag_arena_split==1 ) //  both arena and weight splitting done for the block
//    {
//        printf("both arena and weight splitting\n");
//
//    }

    printf("Completed\n");
    //std::filesystem::create_directory(network_block_name);
    //std::filesystem::create_directory(network_block_name"Node1");





//



//printf("Arena split: %d\n",flag_arena_split);
//printf("Weight split: %d\n",flag_weight_split);




    //fs::create_directories("./a/b/c");
//    fs::create_directories(fpath1);
//    fs::create_directories(fpath1);

    //fs::copy("./mobilenetv1/layerwise/model_layerwise_split_layer_1.tflite", "./mobilenetv1_block1/Node1/model.tflite"); // copy file








}
