### This folder contains automated C++ script for cross-fusion partitioning of any Tensorflow lite (tflite/flatbuffer) Convolution (Conv) neural network generating the cross-fused model

---

#### 1. Overview

This code implements an automated cross-fusion partitioning of initial layers for any tflite CNN model using flatbuffer [[1]](#1) C++ manipulation. it is based on clion IDE.
---

#### 2. Dependencies

###### software

- clion IDE 2019.3.2 [[2]](#2)
---

###### Others:

- Netron for visualizing Networks [[3]](#3)

#### 2. Steps

###### Execution:

Download/clone the "Convolution_Cross_fusion" folder in your machine.
Place the tflite/flatbuffer CNN model you want to partition for cross-fusion of initial layers in the "Convolution_Cross_fusion" folder and rename it to "model.tflite". 
Execute main.cpp to generate the tflite/flatbuffer the cross-fusion models for the initial layers. Partitioned models will also be generated in the "Convolution_Cross_fusion" folder using the name(s): "model_conv_cross_fusion".
   
---  

## References
<a id="1">[1]</a>
https://google.github.io/flatbuffers/index.html 

<a id="2">[2]</a> 
https://www.jetbrains.com/clion/

<a id="3">[3]</a> 
https://github.com/lutzroeder/netron