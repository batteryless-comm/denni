# Tensorflow-lite inference tensor-arena estimation for linear neural network models
Goal of the script is to allow accurately estimating the tensor-arena (intermediate buffer memory) inference memory of linear int8 quantized neural network graph (stored in .tflite format) or its partitions.

#Steps to Run
- To estimate tensor arena for a given model.tflite execute the script arena_estimation.ipynb (google colab or jupyter notebook) or the script arena_estimation.py
