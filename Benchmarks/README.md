# Benchmarks (all integer 8-bit) used in the paper


# DSCNN 
- Keyword spotting CNN taken from https://github.com/ARM-software/ML-examples/tree/main/tflu-kws-cortex-m/Pretrained_models/DS_CNN/DS_CNN_L

# HAR
- We implemented (trained and quantized) CNN without statistical feature addition and data normalization achieving 
95.32% accuracy on the UCI Activity dataset which is within 0.1% of reported accuracy.
- We used avgpooling instead of maxpooling.
- The code is available inside the folder Benchmarks/HAR

# HAR References:
- PAPER: A. Ignatov, “Real-time human activity recognition from accelerometer
data using convolutional neural networks,” Applied
Soft Computing, vol. 62, pp. 915–922, 2018.
- CODE: https://github.com/aiff22/HAR

# Mobilenet CNN's
- Used to implement ImageNet classification on mobile devices. 
- We choose a 128*128*3 int8 scaled down version of ImageNet classify pretrained and int8 quantized mobilenets 
with increasing depth multiplier parameter values of 0.25, 0.50, 0.75 and 1 respectively. 
- We used MNv1, MNv2, MNv3 and MNv4 to denote the mobilent classifiers with depth multiplier parameter values of 0.25, 0.50, 0.75 and 1 respectively
- All mobilenet CNN's (MNv1, MNv2, MNv3 and MNv4 are pretrained and int8 quantized mobilenet refers to MobileNet_v1_0.25_128,  ) 
were taken fromm https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet_v1.md

 
