### Human activity recognition from accelerometer data using Convolutional Neural Networks

---

#### 1. Overview

This code implements a Convolutional Neural Network without statistical feature addition and data normalization achieving 
95.32% test accuracy on the UCI Activity dataset which is within 0.1% of reported accuracy for conventional CNN implementation.
We refer the paper [[1]](#1) and code [[2]](#2) for our implementation.

---

#### 2. Dependencies

###### Data segmentation:

- matlab or octave

###### Activity classification

- python 3.6+
- tensorflow 2.1.0
- keras
- scikit-learn
- numpy
---

###### Others:

- Jupyter Notebook
- Netron for visualizing Networks [[3]](#3)

#### 2. Steps

###### Data segmentation:

Clone the repository [[2]](#2) and unzip file  
"data_processing/datasets.zip" and using matlab execute the script run_UCI.m for segmenting UCI dataset. 

###### Training:

Run the script "HAR_train.ipynb" which will save the complete trained model in keras hdf5 format as "HAR_CNN.h5".

###### Convert to Tensorflow_lite

Run the script "HAR_h5_to_tflite.ipynb" which will convert the hdf5 "HAR_CNN.h5" model to signed int 8-bit quantized model which can be deployed to msp430fr5994.
---  

## References
<a id="1">[1]</a> 
A. Ignatov, “Real-time human activity recognition from accelerometer
data using convolutional neural networks,” Applied
Soft Computing, vol. 62, pp. 915–922, 2018.

<a id="2">[2]</a> 
https://github.com/aiff22/HAR

<a id="3">[3]</a> 
https://github.com/lutzroeder/netron