from __future__ import print_function
import flatbuffers
import tflite.Model
import sys
import numpy as np
from gekko import GEKKO
import math
from pathlib import Path


# maps common operations their its params size in bytes.
# to add an operations, run any tflite program, identify 
# the builtin data struct (check flatbuffer_conversions.cc) 
# and use the following debug expression: sizeof(YOURTYPE)
op_data_sizes = {
    0: 2, # add
    1: 20, # average pool 2d
    2: 4, # concatenation
    3: 12, # conv 2d 
    4: 14, # depthwise conv
    5: 2, # depth to space
    6: 0, # dequantize
    9: 6, # fully connected
    22: 18, # reshape
    25: 4, # softmax
    60: 32, # padv2, not completely sure here
    114: 0, # quantize
}

op_names = [
   "ADD","AVERAGE_POOL_2D","CONCATENATION","CONV_2D","DEPTHWISE_CONV_2D",
   "DEPTH_TO_SPACE","DEQUANTIZE","EMBEDDING_LOOKUP","FLOOR","FULLY_CONNECTED",
   "HASHTABLE_LOOKUP","L2_NORMALIZATION","L2_POOL_2D","LOCAL_RESPONSE_NORM","LOGISTIC",
   "LSH_PROJECTION","LSTM","MAX_POOL_2D","MUL","RELU",
   "RELU_N1_TO_","RELU","RESHAPE","RESIZE_BILINEAR","RNN",
   "SOFTMAX","SPACE_TO_DEPTH","SVDF","TANH","CONCAT_EMBEDDINGS",
   "SKIP_GRAM","CALL","CUSTOM","EMBEDDING_LOOKUP_SP","PAD",
   "UNIDIRECTIONAL_SEQU","GATHER","BATCH_TO_SPACE_ND","SPACE_TO_BATCH_ND","TRANSPOSE",
   "MEAN","SUB","DIV","SQUEEZE","UNIDIRECTIONAL_SEQU",
   "STRIDED_SLICE","BIDIRECTIONAL_SEQUE","EXP","TOPK_V","SPLIT",
   "LOG_SOFTMAX","DELEGATE","BIDIRECTIONAL_SEQUE","CAST","PRELU",
   "MAXIMUM","ARG_MAX","MINIMUM","LESS","NEG",
   "PADV","GREATER","GREATER_EQUAL","LESS_EQUAL","SELECT",
   "SLICE","SIN","TRANSPOSE_CONV","SPARSE_TO_DENSE","TILE",
   "EXPAND_DIMS","EQUAL","NOT_EQUAL","LOG","SUM",
   "SQRT","RSQRT","SHAPE","POW","ARG_MIN",
   "FAKE_QUANT","REDUCE_PROD","REDUCE_MAX","PACK","LOGICAL_OR",
   "ONE_HOT","LOGICAL_AND","LOGICAL_NOT","UNPACK","REDUCE_MIN",
   "FLOOR_DIV","REDUCE_ANY","SQUARE","ZEROS_LIKE","FILL",
   "FLOOR_MOD","RANGE","RESIZE_NEAREST_NEIG","LEAKY_RELU","SQUARED_DIFFERENCE",
   "MIRROR_PAD","ABS","SPLIT_V","UNIQUE","CEIL",
   "REVERSE_V","ADD_N","GATHER_ND","COS","WHERE",
   "RANK","ELU","REVERSE_SEQUENCE","MATRIX_DIAG","QUANTIZE",
   "MATRIX_SET_DIAG","ROUND","HARD_SWISH","IF","WHILE",
   "NON_MAX_SUPPRESSION","NON_MAX_SUPPRESSION"
]

def calculate_planned_area(model):
    """ recreation of the greedy memory planner algorithm to obtain max size """
    # since our models are linear, meaning the subgraph is a straight line 
    # with only 1 allocated tensor per layer. We don't need to implement
    # the full algorithm since the planned arena will just be the sum
    # of the consecutive tensors.

    subg = model.Subgraphs(0)

    def tensor_size_alligned_up(i):
        # tflite type enum to size in bytes
        type_sizes = {0:4,1:2,2:4,3:1,4:8,6:1,7:2,8:8,9:1}
        raw = np.prod(subg.Tensors(i).ShapeAsNumpy())
        extra = raw%16
        length = raw if extra==0 else 16*(np.math.floor(raw/16)+1)
        assert subg.Tensors(i).Type() in type_sizes, "Unsupported tflite type enum value {subg.Tensors(i).Type()}"
        return type_sizes[subg.Tensors(i).Type()] * length

    def needs_allocating(i):
        buffer_idx = subg.Tensors(i).Buffer()
        return model.Buffers(buffer_idx).DataLength() == 0 and not subg.Tensors(i).IsVariable()

    tensors = list(range(subg.TensorsLength()))
    # map to the format (tensors, size, first used, last used) for variable tensors only
    tensors = [[t,tensor_size_alligned_up(t),-1,-1] for t in tensors]
    
    assert subg.OutputsLength() == 1 and subg.InputsLength() == 1, "Subgraph has more than 1 input/output"
    tensors[subg.Outputs(0)][3] = subg.OperatorsLength()-1
    tensors[subg.Inputs(0)][2] = 0

    # find first used
    for o in range(subg.OperatorsLength()):
        for t in subg.Operators(o).OutputsAsNumpy():
            tensors[t][2] = o

    # find last used
    for o in range(subg.OperatorsLength()):
        for t in subg.Operators(o).InputsAsNumpy():
            tensors[t][3] = o

    # filter out tensors we dont need to allocate
    tensors = [t for t in tensors if needs_allocating(t[0])]

    # Use simplified algorithm described above
    # sort tensors by usage (in a way we can check that its linear)
    tensors.sort(key=lambda t: t[2] + 10000*t[3])
    assert len(tensors) > 0, "Model has no allcoated tensors. Is this a real model?"
    max_size = 0
    for i in range(len(tensors)-1):
        local_size = tensors[i][1] + tensors[i+1][1]
        if max_size < local_size:
            max_size = local_size

    return max_size


def calculate_arena_size(model):
    """
    Calculates the minimum tensor_arena size in bytes for a given tflite model
    to be run on the MSP430
    """
    subg = model.Subgraphs(0)
    tensor_len = subg.TensorsLength()
    operations_len = subg.OperatorsLength()

    node_and_registration_size = 0
    builtin_data_size = 0
    tensor_size = 0
    quantization_param_size = 0
    planned_area = calculate_planned_area(model)

    for i in range(operations_len):
        node_and_registration_size += 38 # Node and Registration
        operation_id = model.OperatorCodes(subg.Operators(i).OpcodeIndex()).BuiltinCode()
        operation_name = op_names[operation_id]
        assert operation_id in op_data_sizes, f"please add size of {operation_name}"
        tensor_builtin_data_size = op_data_sizes[operation_id]
        builtin_data_size += tensor_builtin_data_size
        # print(operation_name, tensor_builtin_data_size)

    for i in range(tensor_len):
        tensor_size += 50 # runtime tensor
        quant = subg.Tensors(i).Quantization()
        if quant:
            quantization_param_size += 12 # TfLiteAffineQuantization wrapper
            channels = quant.ScaleLength()
            quantization_param_size += 4*(channels+1) # zero points as a TfLiteIntArray
            quantization_param_size += 4*(channels+1) # scales as a TfLiteFloatArray

    total = node_and_registration_size + builtin_data_size + tensor_size + quantization_param_size + planned_area + 50
    #print(quantization_param_size)
    total = total

    return total



# flag layer_27_28 decides if the layer is conv layer 27, 28 or not. If layer_27_28==1 then layer is 27 and layer_27_28=2 then layer is 28 and if layer_27_28==0, layer is not 27 or 28 
# Layer 27 and 28 are later layers where multiple operators are present.
# Layer 27: Conv + Avgpool with code size: 119024
# Layer 28: Conv + Reshape + Softmax: 132838
# Both are convolution layers so we only add them to conv logic
def Min_nodes_layer(model,model_mem_size,flag_first_chunk,layer_27_28, Node_memory,dispflag):
    # Read the tflite model and extract the Input dimensions: H, W, N, filter dim: R,S,N,M, Output dim: E, F, X, Baseline model size: B (in bytes)
    
    subg = model.Subgraphs(0)

    # Get the input dimensions of the layer
    tensor_input=subg.Operators(0).Inputs(0)
    H = subg.Tensors(tensor_input).Shape(1)
    W = subg.Tensors(tensor_input).Shape(2)
    N = subg.Tensors(tensor_input).Shape(3)



    # Get the dimenisons of the weights of the layer
    tensor_weights=subg.Operators(0).Inputs(1)
    R= subg.Tensors(tensor_weights).Shape(1)
    S= subg.Tensors(tensor_weights).Shape(2)
    N= subg.Tensors(tensor_weights).Shape(3)
    M= subg.Tensors(tensor_weights).Shape(0)

    # Get the dimensions of the output of the layer
    tensor_output=subg.Operators(0).Outputs(0)
    E = subg.Tensors(tensor_output).Shape(1)
    F = subg.Tensors(tensor_output).Shape(2)

    # Get the base model input and arena size
    Base_model_arena= calculate_arena_size(model)
    Base_model_input= H*W*N
    Base_model_weights=0

    
    # Get the layer operator type: CONV2D or DEPTHWISE CONV2D
    operation_id = model.OperatorCodes(subg.Operators(0).OpcodeIndex()).BuiltinCode()
    if operation_id==3 and layer_27_28==0: # CONV2D
        #code_size = 114226 # In Bytes for 256 KB
        code_size = 108334 # In Bytes for 213 KB: 99632 without padv2, 108334 with conv and padv2
        M= subg.Tensors(tensor_weights).Shape(0)
        Base_model_weights=R*S*N*M+4*M
        # Arena offset will be added to the planned area to get the size
        Arena_offset = 412 # In Bytes...Although the quantization params for zero point and scale: 4*(out_channel+1) would change...neglecting this value as the change is small compared to planned area
    
    elif operation_id==3 and layer_27_28==1: # CONV2D and layer 27 with avgpool operator
        #code_size = 119024 # In Bytes for 256 KB
        code_size = 108184 # In Bytes
        #M = subg.Tensors(tensor_output).Shape(3)
        M= subg.Tensors(tensor_weights).Shape(0)
        Base_model_weights=R*S*N*M+4*M
        #print(M)
        # Arena offset will be added to the planned area to get the size
        Arena_offset = 412 # In Bytes...Although the quantization params for zero point and scale: 4*(out_channel+1) would change...neglecting this value as the change is small compared to planned area
    
    elif operation_id==3 and layer_27_28==2: # CONV2D and layer 28 having conv, reshape and softmax opeator
        #code_size = 132838 # In Bytes for 256 KB
        code_size = 118438 # In Bytes for 213 KB
        #M = subg.Tensors(tensor_output).Shape(3)
        M= subg.Tensors(tensor_weights).Shape(0)
        Base_model_weights=R*S*N*M+4*M
        #print(M)
        # Arena offset will be added to the planned area to get the size
        Arena_offset = 412 # In Bytes...Although the quantization params for zero point and scale: 4*(out_channel+1) would change...neglecting this value as the change is small compared to planned area
    

    elif operation_id==4: # DEPTHWISE_CONV2D
        code_size = 114064 # In Bytes for 256 KB
        code_size = 106926 # In Bytes for 213 KB: 98224 without padv2, 106926 with padv2
        #M = subg.Tensors(tensor_output).Shape(0)
        Base_model_weights=R*S*M*N+4*N
        #M = subg.Tensors(tensor_weights).Shape(3)
        #print(M)
        # Arena offset will be added to the planned area
        Arena_offset = 414 # In Bytes...Although the quantization params for zero point and scale: 4*(out_channel+1) would change...neglecting this value as the change is small compared to planned area

    else:
        print("Error only CONV2D and DEPTHWISE CONV2D operator supported!")

    #Initialize Model
    m = GEKKO(remote=False)

    m.options.SOLVER=1  # APOPT is an MINLP solver

    #Mem_node = m.Const(value=Node_memory)
    Mem_node = m.Param(value=Node_memory)

    
    # Initialize variables
    p = m.Var(value=1,lb=1,ub=E,integer=True)
    q = m.Var(value=1,lb=1,ub=F,integer=True)
    if operation_id==4: # DEPTHWISE_CONV2D
        x = m.Var(value=1,lb=1,ub=N,integer=True) # changed value from 1 to N
    else:
        x = m.Var(value=1,lb=1,ub=M,integer=True)

    
    if operation_id==3: # CONV2D
    # Finding U, *** using a trick here if the output of baseline layer is half of the input then U=2 else it is 1
        if(E==H/2 and F==W/2):
            U=2
        elif(E==int(H/2+1)):# added for dscnn
            U=2
        else:
            U=1
    # Calculating arena size based on the updated model output. Input channel would remain same for convolution 

        if U==2:
            Total_Arena_Size = (2*m.max2(((U*(p)+1)*(U*(q)+1)*N),p*q*x))+Arena_offset

           
        elif U==1 and R==3 and S==3:
            Total_Arena_Size= (2*m.max2(((p+2)*(q+2)*N),p*q*x))+Arena_offset
        elif U==1 and R==1 and S==1:
            Total_Arena_Size= (2*p*q*N)+Arena_offset


    if operation_id==4: # DEPTHWISE CONV2D
    # Finding U, *** using a trick here if the output of baseline layer is half of the input then U=2 else it is 1
        if(E==H/2 and F==W/2):
            U=2
        elif(E==int(H/2+1)):# added for dscnn
            U=2
        else:
            U=1
    # Calculating arena size based on the updated model output. Input channel will be x in case of depthwise convolution

        if U==2:
            Total_Arena_Size = m.Intermediate(2*m.max2(((U*(p)+1)*(U*(q)+1)*x),p*q*x))+Arena_offset
           
        elif U==1 and R==3 and S==3:
            Total_Arena_Size= m.Intermediate(2*m.max2(((p+2)*(q+2)*x),p*q*x))+Arena_offset
        elif U==1 and R==1 and S==1:
            Total_Arena_Size= m.Intermediate(2*p*q*x)+Arena_offset


    # Calculate the updated model size of each partition.  Since the output channel per node are now x instead of M. The model size of each partition essentially gets divided by x
    # t1= m.Intermediate(M/x)
    # # Model_updated_Size= model_mem_size/(M/x)
    # Model_updated_Size= m.Intermediate(model_mem_size/(t1))

    if operation_id==4: # DEPTHWISE CONV2D
        model_weights_upt= m.Intermediate(R*S*M*x+4*x)
    else:
        model_weights_upt= m.Intermediate(R*S*N*x+4*x)

    
   # Added code
    if(Base_model_arena>model_mem_size): # more chance of arena splitting and so we add the  model offset due to PAD operator
        Model_updated_Size = m.Intermediate(model_mem_size-Base_model_weights+model_weights_upt+340)
    else:
        Model_updated_Size = m.Intermediate(model_mem_size-Base_model_weights+model_weights_upt)


        # so input size in bytes
    if flag_first_chunk==1: # if it is the first layer, we need to store the complete input    
        input_size = m.Intermediate(H*W*N)
    else: # if it is any other layer then we just need to store the input needed for cross fusion which may be provided by previous layer
        if operation_id==3: # CONV2D
    # Finding U, *** using a trick here if the output of baseline layer is half of the input then U=2 else it is 1
            if(E==H/2 and F==W/2):
                U=2
            else:
                U=1
        # Calculating input size based on the updated model output

            if U==2:
                input_size = m.Intermediate((U*(p)+1)*(U*(q)+1)*N)
            elif U==1 and R==3 and S==3:
                input_size = m.Intermediate((p+2)*(q+2)*N)
            elif U==1 and R==1 and S==1:
                input_size = m.Intermediate((p)*(q)*N)

        if operation_id==4: # DEPTHWISE CONV2D
    # Finding U, *** using a trick here if the output of baseline layer is half of the input then U=2 else it is 1
            if(E==H/2 and F==W/2):
                U=2
            else:
                U=1
        # Calculating input size based on the updated model output. Input channel will be x in case of depthwise convolution

            if U==2:
                input_size = m.Intermediate((U*(p)+1)*(U*(q)+1)*x)
               
            elif U==1 and R==3 and S==3:
                input_size = m.Intermediate((p+2)*(q+2)*x)
            elif U==1 and R==1 and S==1:
                input_size = m.Intermediate((p)*(q)*x)
    


    # To implement the ceil function
    Ei = m.Var(integer=True)

    m.Equation(Ei-(E/p)==0) # to make it integer

   

    Fi = m.Var(integer=True)

    m.Equation(Fi-(F/q)==0) # to make it integer

  
    Xi = m.Var(integer=True)

   

    if operation_id==4: # DEPTHWISE_CONV2D
        m.Equation(Xi-(N/x)==0) # to make it integer
    else:
        m.Equation(Xi-(M/x)==0) # to make it integer

    

    
    n1=m.Intermediate(Ei)
    n2=m.Intermediate(Fi)
    n3=m.Intermediate(Xi)

    Total_nodes=m.Intermediate(n1*n2*n3)




    # check that total arena should not exceed base arena. Was min originally
    Total_Arena_eff = m.min2(Total_Arena_Size,Base_model_arena)

    # check that total input should not exceed base input
    Total_Input_eff = m.min2(input_size,Base_model_input)

    #Equations
    m.Equation(Total_Arena_eff + Model_updated_Size + code_size + Total_Input_eff <= Mem_node)


   

    m.Obj(Total_nodes)

    

    #m.open_folder()
    try:
        m.solve(disp=False) # Set display to false
        if dispflag==1:
           # print("For Layer: "+str(i))
            #print("Minimum Nodes required: "+str(math.floor(m.options.objfcnval)))
            print('')
            print('Results')
            print("Total nodes: "+str(Total_nodes.VALUE))
            print('E: '+str(E))
            print('p: ' + str(p.VALUE))
            print('F: '+str(F))
            print('q: ' + str(q.VALUE))
            if operation_id==4: # DEPTHWISE_CONV2D
                print('N: '+str(N))
            else:
                print('M: '+str(M))
            print('x: ' + str(x.VALUE))
            # print("U"+str())
            # print('n1: ' + str(n1.VALUE))
            # print('n2: ' + str(n1.VALUE))
            # print('n3: ' + str(n1.VALUE))
            print("Flag first chunk: "+str(flag_first_chunk))
            if(flag_first_chunk==1):
                print("Input :"+str(Total_Input_eff.VALUE))
            else:
                print("Input :"+str(Total_Input_eff.VALUE))

            print("Arena :"+str(Total_Arena_eff.VALUE))
            print("Model updated :"+str(Model_updated_Size.VALUE))
            #print("Model base: "+str(model_base_size))
            print("Code: "+str(code_size))
    except:
        print("An exception occurred in layerwise min")
  

        

    return math.floor(m.options.objfcnval)




def Min_Layerwise_i_j(i,j, Network_to_run,Node_Memory,printflag): #prints the minimum node summing up layer i to layer j minimum
    total_min_nodes=0    
    for k in range(i,j+1):
        Network_path=get_path_layerwise(Network_to_run,k)
        buf=open(Network_path,'rb').read()
        model = tflite.Model.GetRootAsModel(buf, 0)
        model_base_size=sys.getsizeof(buf)

        if(k==1): # if first layer
            total_min_nodes += Min_nodes_layer(model,model_base_size,1,0,Node_Memory,printflag)
        elif (k<27): # if not the 27 or 28 layer
            total_min_nodes += Min_nodes_layer(model,model_base_size,0,0,Node_Memory,printflag)
        elif (k==27): # it is the 27 layer
            total_min_nodes += Min_nodes_layer(model,model_base_size,0,1,Node_Memory,printflag)
        else: # 28 layer
            total_min_nodes += Min_nodes_layer(model,model_base_size,0,2,Node_Memory,printflag)
    return total_min_nodes


def Min_Layerchunks_i_j(i,j,Network_to_run,Node_memory,printflag): #prints the minimum node for layer chunk from layer i to layer j
    total_min_nodes=0
    Network_path=get_path_chunk(Network_to_run,i,j)
    buf=open(Network_path,'rb').read()
    model = tflite.Model.GetRootAsModel(buf, 0)
    model_base_size=sys.getsizeof(buf)
    sum_layer=Get_sum_model_i_j(i,j,Network_to_run)
    if(i==1): # chunk contains the first layer
            r = Cross_fuse_layer_chunks(model,model_base_size,1,0,Node_memory,sum_layer,printflag)
            total_min_nodes += r
            if(r==-1):
                print("An exception occurred at layer chunk: "+str(i)+"_to_"+str(j))
                return -1
            
    elif(i!=1 and j<27): # if chunk does not contain first layer and is less than 27
            r =  Cross_fuse_layer_chunks(model,model_base_size,0,0,Node_memory,sum_layer,printflag)
            total_min_nodes += r
            if(r==-1):
                print("An exception occurred at layer chunk: "+str(i)+"_to_"+str(j))
                return -1
    elif(i!=1 and j==27): # if chunk does not contain first layer but contains 27 as the end layer
        r =  Cross_fuse_layer_chunks(model,model_base_size,0,1,Node_memory,sum_layer,printflag)
        total_min_nodes += r
        if(r==-1):
            print("An exception occurred at layer chunk: "+str(i)+"_to_"+str(j))
            return -1

    elif(i!=1 and j==28): # if chunk does not contain first layer but contains 28 as the end layer
        r =  Cross_fuse_layer_chunks(model,model_base_size,0,2,Node_memory,sum_layer,printflag)
        total_min_nodes += r
        if(r==-1):
            print("An exception occurred at layer chunk: "+str(i)+"_to_"+str(j))
            return -1
    
         
    return total_min_nodes

# HAR layer 2 MINLP because only layer2 dominate in HAR benchmark
def HAR_L2(Mem):
    #Initialize Model
    m = GEKKO()

    m.options.SOLVER=1  # APOPT is an MINLP solver

    Mem_node = m.Const(value=Mem)

    code = m.Const(value=84956) # just for fully connected

    input_size = m.Const(value=6272)

    Arena = m.Const(value=(7*1024))

    X = m.Var(value=1024,lb=1,ub=1024,integer=True)

    Weights_layer2 = 6272*X+4*X

    metadata= 35392

    model_updated_mem_l2=m.Intermediate(metadata+Weights_layer2)

    m.Equation(code+ Arena + model_updated_mem_l2 + input_size<=Mem_node)

    W = m.Var(integer=True)

    m.Equations([(1024/X)<=W, W<=(1024/X)+0.999])

    m.Obj(W)
    

    try:
        m.solve(disp=False) # Set display to false
    except:
        print("An exception occurred in layerwise chunk")
        return -1
    #return X.VALUE[0]
    return W.VALUE
   



def Minimize(i,j,Network_to_run,Node_Memory): # find till when the chunk i,j provides more minimum node than layer i,j, return 0 when it succeed...breaks when it fails returning the value of j where it fails
   
    if(j==(i+1)):
        if( Min_Layerchunks_i_j(i,j,Network_to_run,Node_memory,0)==-1): # Layer chunk failed to solve
            return j
        else:
            if((Min_Layerchunks_i_j(i,j,Network_to_run,Node_memory,0)<=Min_Layerwise_i_j(i,j,Network_to_run,Node_Memory,0))):
                return 0
            else:
                return j
    if(j>(i+1) or j==i):
        if( Min_Layerchunks_i_j(i,j,Network_to_run,Node_memory,0)==-1): # Layer chunk failed to solve
            return j
        else:
            if((Min_Layerchunks_i_j(i,j,Network_to_run,Node_memory,0)<=Min_Layerwise_i_j(i,j,Network_to_run,Node_Memory,0)) and (Min_Layerchunks_i_j(i,j,Network_to_run,Node_memory,0)<=Min_Layerchunks_i_j(i,j-1,Network_to_run,Node_memory,0)+Min_Layerwise_i_j(j,j,Network_to_run,Node_Memory,0))):
                return 0
            else:
                return j




        







def Get_Base_Conv_DW_layer_params(model,op_index):
    subg = model.Subgraphs(0)
    tensor_input=subg.Operators(op_index).Inputs(0)
    # Get the operator id to determine if it is convolution or depthwise conv
    operation_id = model.OperatorCodes(subg.Operators(op_index).OpcodeIndex()).BuiltinCode()
    type=0
    U=0
    if(operation_id==1):
        type=3 # it is avgpool and since avgpool does not have weights, we will skip them and mark them 0
            # Get the input dimensions of the layer
        H = subg.Tensors(tensor_input).Shape(1)
        W = subg.Tensors(tensor_input).Shape(2)
        N = subg.Tensors(tensor_input).Shape(3)
        M = subg.Tensors(tensor_input).Shape(0)

        # No weights
        R=0
        S=0

        # Get the dimensions of the output of the layer
        tensor_output=subg.Operators(op_index).Outputs(0)
        E = subg.Tensors(tensor_output).Shape(1)
        F = subg.Tensors(tensor_output).Shape(2)

        U=2

        return H, W, R, S, N, M, U, E, F, type


    elif(operation_id==3 or operation_id==4): # if conv2d or depthwise conv2d
        tensor_input=subg.Operators(op_index).Inputs(0)
        # Get the input dimensions of the layer
        H = subg.Tensors(tensor_input).Shape(1)
        W = subg.Tensors(tensor_input).Shape(2)

        # Get the dimenisons of the weights of the layer
        tensor_weights=subg.Operators(op_index).Inputs(1)
        R= subg.Tensors(tensor_weights).Shape(1)
        S= subg.Tensors(tensor_weights).Shape(2)
        N= subg.Tensors(tensor_weights).Shape(3)
        M= subg.Tensors(tensor_weights).Shape(0)

        # Get the dimensions of the output of the layer
        tensor_output=subg.Operators(op_index).Outputs(0)
        E = subg.Tensors(tensor_output).Shape(1)
        F = subg.Tensors(tensor_output).Shape(2)

        # determine stride U
        # Finding U, *** using a trick here if the output of baseline layer is half of the input then U=2 else it is 1
        if(E==H/2 and F==W/2):
            U=2
        elif(E==int(H/2+1)):# added for dscnn
            U=2
        else:
            U=1

       

        if(operation_id==3):
            type=0 # it is convolution 2d

        if(operation_id==4):
            type=1 # it is depthwise conv

    
        return H, W, R, S, N, M, U, E, F, type
    else:
        print("operation id is not conv, dc or avgpool")


def Get_Base_Input_Weights_Output_Arena_stride_outc_split(model,op_index):
    H, W, R, S, N, M, U, E, F, type = Get_Base_Conv_DW_layer_params(model,op_index)
    inputs = H*W*N



    if(type==0): # Conv
        weights= R*S*N*M+4*M
        outputs= E*F*M
        #arena_offset=412
        outc_split=M

    if(type==1): # Depthwise conv
        weights= R*S*N*M+4*N
        outputs= E*F*M
        #arena_offset=414
        outc_split=N

    if(type==3): # Avgppol
        weights= 0
        outputs= E*F*N
        #arena_offset=414
        outc_split=N

    arena = inputs+outputs

    return inputs, weights, outputs, arena, U, outc_split

def Get_sum_model_i_j(i,j,Network_to_run): # get the sum of the model sizes for the layer i to j
    sum_model=0
    for k in range(i,j+1):
        Network_path=get_path_layerwise(Network_to_run,k)
        buf=open(Network_path,'rb').read()
        model = tflite.Model.GetRootAsModel(buf, 0)
        sum_model+=sys.getsizeof(buf)
        #print(sys.getsizeof(buf))
    return sum_model





# This only works for arena dominated layer of mobilenet
def Get_Arena_offset_layer_i_and_j(i,j): # This function approximates the arena offset to be added to planned area for acurate arena size calculation
    # Simple estimation is: if graph has L layers/ops then each contributes (38(Node&regis)+14(opcode data)) bytes and the (L*3+1) tensors for L layers would contribute...(8(scale)+8(zero point)+12(Quant wrapper)+50(Runtime tensor))
    # Assuming i is indexed starting from 1
    #error_tolerance=1024 # in bytes for any error creeping in
    L= j-i+1
    #offset_arena = L*(38+14) + (L*3+1)*(8+8+12+50)+error_tolerance
    offset_arena = L*(38+14) + (L*3+1)*(8+8+12+50)
    return offset_arena



def Get_H_W_prev_layer(E,F,R,S,U):
    if U==2 and R==3 and S==3:
        H= U*E+1
        W= U*F+1

    elif U==1 and R==3 and S==3:
        H= E+2
        W= F+2

    elif U==1 and R==1 and S==1:
        H= E
        W= F 
    
    else: # for avgpool
        H=U*E
        W=U*F

    return H, W  



# layer27_28 decides if the layer chunk contains layer 27 or 28.  if it is 0...it does not contain layer 27,28, if it is 1 it contains layer 27, if it is 2 it contains layer 27 and 28
def Cross_fuse_layer_chunks(model, model_mem_size, flag_first_chunk,layer27_28,Node_memory,sum_mem_layer,dispflag):

    #Initialize Model
    m = GEKKO(remote=False)

    m.options.SOLVER=1  # APOPT is an MINLP solver

    Mem_node = m.Const(value=Node_memory)

    #Mem_node = m.Param(value=Node_memory)

    # Get the params of last layer to constrain p,q
    subg = model.Subgraphs(0)
    if(layer27_28==0 or layer27_28==1):
        L = subg.OperatorsLength()-1 # No_of_layers...start from the last layer
    elif(layer27_28==2):
        L = subg.OperatorsLength()-3 # No_of_layers...start from the last layer-2 to avoid softmax and reshape

    #last_layer_indx = (subg.OperatorsLength()-1)
    H, W, R, S, N, M, U, E, F, type = Get_Base_Conv_DW_layer_params(model,L)

    if(layer27_28==0 or layer27_28==1):
        L=L+1


    # Get the baseline params

    # Var to store baseline params
    Inputs_base=np.zeros(L)
    Outputs_base=np.zeros(L)
    Arena_base=np.zeros(L)
    Weights_base=np.zeros(L)
    Outc_split=np.zeros(L)
    stride_base=np.zeros(L)

    # Var to store updated cross fused GEKKO params
    Inputs_upt= m.Array(m.Var,(L))
    Outputs_upt= m.Array(m.Var,(L))
    Arena_upt= m.Array(m.Var,(L))
    Weights_upt= m.Array(m.Var,(L))

    for i in range(L):
        Inputs_base[i], Weights_base[i], Outputs_base[i], Arena_base[i], stride_base[i] , Outc_split[i] = Get_Base_Input_Weights_Output_Arena_stride_outc_split(model,i)




    # Initialize variables p,q using output dimensions of last layer

    p = m.Var(value=1,lb=1,ub=int(E),integer=True)
    q = m.Var(value=1,lb=1,ub=int(F),integer=True)

    Ei = m.Var(integer=True)
    Fi = m.Var(integer=True)


    #initialize list to store split output channel variable per layer. X[i] will store variable for layer i
    X=[]
    for i in range(L):
        X.append(m.Var(value=Outc_split[i],lb=1,ub=Outc_split[i],integer=True)) 

    m.Equation(Ei-(E/p)==0) # to make it integer

    m.Equation(Fi-(F/q)==0) # to make it integer


    # original code
    nodes_cross_fuse=m.Intermediate((Ei)*(Fi))

   
    #nodes due to weight splitting (Max)
    nodes_weight_split=m.Intermediate(Outc_split[0]/X[0])
    for i in range(1,L):
        z=m.max2(nodes_weight_split,m.Intermediate(Outc_split[i]/X[i]))
        nodes_weight_split=z

    Wi = m.Var(integer=True)

    m.Equation(Wi-(nodes_weight_split)==0) # to make it integer

    Total_number_nodes = m.Intermediate((nodes_cross_fuse)* (Wi))


    # Variables to store the previous layer output. if it the last layer then they are just p,q. So we start from the last layer. 
    Prev_L_E = p
    Prev_L_F = q
   


    # starting from the last layer track input, output and weights
    for l in reversed(range(0,L)):
        # first get the parameters of baseline current layer
        H, W, R, S, N, M, U, E, F, type = Get_Base_Conv_DW_layer_params(model,l)

        if(type==0): # if the layer is convolution
            H,W = Get_H_W_prev_layer(Prev_L_E,Prev_L_F,R,S,U)
            Inputs_upt[l] = H*W*N # since we are not splitting input channels in conv...it would need all of them in input
            Outputs_upt[l] = Prev_L_E*Prev_L_F*X[l]
            Weights_upt[l]=m.Intermediate(R*S*N*X[l]+4*X[l]) # 1736
            Prev_L_E=H
            Prev_L_F=W

        if(type==1): # if the layer is Depthwise convolution
            H,W = Get_H_W_prev_layer(Prev_L_E,Prev_L_F,R,S,U)
            Inputs_upt[l] = H*W*X[l] # Since we are splitting by input channel..so the input will be modified acc
            Outputs_upt[l] = Prev_L_E*Prev_L_F*X[l]
            Weights_upt[l]=m.Intermediate(R*S*M*X[l]+4*X[l]) # 1746
            Prev_L_E=H
            Prev_L_F=W

        if(type==3): # Avgpool
            H,W = Get_H_W_prev_layer(Prev_L_E,Prev_L_F,R,S,U)
            Inputs_upt[l] = H*W*X[l] # Since we are splitting by input channel..so the input will be modified acc
            Outputs_upt[l] = Prev_L_E*Prev_L_F*X[l]
            Weights_upt[l]=m.Intermediate(0)
            Prev_L_E=H
            Prev_L_F=W



    Arena_upt[0]=m.max2(Inputs_upt[0],m.max2(Outputs_upt[0],Inputs_upt[1]))
    Arena_upt[L-1]=m.max2(Outputs_upt[L-1],m.max2(Inputs_upt[L-1],Outputs_upt[L-2]))
    for i in range(1,L-1): # for layers 1 to L-2
        Arena_upt[i]=m.max2(m.max2(Inputs_upt[i],Outputs_upt[i-1]),m.max2(Outputs_upt[i],Inputs_upt[i+1]))
    


    Total_Arena_base = calculate_arena_size(model) # Get the exact arena size of baseline model in Bytes  
    
    # find the updated cross fuse arena
    Total_arena_upt1 = Arena_upt[0]
    for i in range(1,L):
        y=m.max2(Total_arena_upt1,Arena_upt[i])
        Total_arena_upt1=y


    
    # Get the double of the of max tensor of the max arena
    Total_arena_upt= m.Intermediate((2*Total_arena_upt1))
   

    # We consider only the code portion in the 213 KB HIFRAM. If layer27_28=0, we will only use conv, DC and PADV2 ~: 115698, if layer27_28=1, we will only use conv, DC and Avgpool:~115548
    # if layer27_28=2, we will use only 134342 bytes for  conv, DC, softmax and reshape
    if(layer27_28==0):
        Total_code = m.Const(115698)
        code=115698
    elif (layer27_28==1): # includes layer 27 conv, depthwise, avgpool
        Total_code = m.Const(115548)
        code=115548
    else: # includes layer 28 conv, depthwise, avgpool
        Total_code = m.Const(134342)
        code=134342


   

    
    if((Total_Arena_base+model_mem_size)>(Node_memory-Inputs_base[0]-code)): # both are split
        Total_arena_eff=Total_arena_upt
        Total_model_eff= m.Intermediate(sum_mem_layer-sum(Weights_base)+m.sum(Weights_upt)+340*(L))
    else:
    
        Total_arena_eff=m.Intermediate(Total_Arena_base)
        Total_model_eff= m.Intermediate(sum_mem_layer-sum(Weights_base)+m.sum(Weights_upt))



    # Input size would be the initial input size of the first layer. To do add logic that for first layer we need complete input but for rest of the layer we need a part of it depending upon p,q
    if(flag_first_chunk==1):
        Total_input_eff = Inputs_base[0] # if it is the first layer chunk
    else:
        Total_input_eff = m.min2(Inputs_upt[0],Inputs_base[0])

   

    
    m.Equation(Total_input_eff + Total_arena_eff+ Total_model_eff + Total_code <= Mem_node)

    
    # so the final objective would be
    m.Obj(Total_number_nodes)

    Total_pyramids=m.Intermediate(Ei*Fi)

    try:
        #m.open_folder()
        m.solve(disp=False) # Set display to false
    except:
        print("An exception occurred in layerwise chunk")
        return -1
 
    if dispflag==1:
        print("Ei: "+str(Ei.VALUE))
        print("Fi: "+str(Fi.VALUE))
        print('p: ' + str(p.VALUE))
        print('q: ' + str(q.VALUE))
        print("Xi is: "+ str(X))
        print("Total Pyramids: "+str(Total_pyramids.VALUE))
        print("Total weight partitions of each pyramid: "+str(Wi.VALUE))
        print("Arena for Max pyramid: "+ str(Total_arena_eff.VALUE))
        print("Model size for max pyramid: "+ str(Total_model_eff.VALUE))
        print("Base arena: "+str(Total_Arena_base))
        #print("Input: "+ str(Total_input_eff.VALUE  ))
        if(flag_first_chunk==1):
            #input_mem=Total_input_eff
            print("Input: "+ str(Total_input_eff))
        else:
            #input_mem=Total_input_eff.VALUE
            print("Input: "+ str(Total_input_eff.VALUE  ))
      

    
    return Total_number_nodes.VALUE[0]


def get_path_chunk(Network_to_run,l,m): # here network decides which network to run and l and m decides the start and stop index of layer chunk
    if(Network_to_run==1):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\mobilenet_0.25_least_layerwise_arena_dominated\Layer_chunks\model_split_layer_'+str(l)+'_to_'+str(m)+'.tflite')
    elif(Network_to_run==2):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\mobilenet_0.5_layerwise_arena_dominated\Layer_chunks\model_split_layer_'+str(l)+'_to_'+str(m)+'.tflite')
    elif(Network_to_run==3):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\mobilenet_0.75_layerwise_arena_dominated\Layer_chunks\model_split_layer_'+str(l)+'_to_'+str(m)+'.tflite')
    elif(Network_to_run==4):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\mobilenet_1_max_layerwise_arena_dominated\Layer_chunks\model_split_layer_'+str(l)+'_to_'+str(m)+'.tflite')
    elif(Network_to_run==5):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\DS_CNN\Layer_chunks\model_split_layer_'+str(l)+'_to_'+str(m)+'.tflite')
    else:
        print("Error network not found!!!")

    return Network



def get_path_layerwise(Network_to_run,l):
    if(Network_to_run==1):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\mobilenet_0.25_least_layerwise_arena_dominated\Layerwise\model_layerwise_split_layer_' +str(l)+'.tflite')
    elif(Network_to_run==2):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\mobilenet_0.5_layerwise_arena_dominated\Layerwise\model_layerwise_split_layer_' +str(l)+'.tflite')
    elif(Network_to_run==3):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\mobilenet_0.75_layerwise_arena_dominated\Layerwise\model_layerwise_split_layer_' +str(l)+'.tflite')
    elif(Network_to_run==4):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\mobilenet_1_max_layerwise_arena_dominated\Layerwise\model_layerwise_split_layer_' +str(l)+'.tflite')
    elif(Network_to_run==5):
        Network=Path(r'C:\Users\mailr\Documents\DENNI_current_research\Results\Plots\Layer_Min_Node_estimate\tflite_flatbuffer_explorer\DS_CNN\Layerwise\model_layerwise_split_layer_' +str(l)+'.tflite')
    else:
        print("Error network not found!!!")
    return Network


def print_layerwise_breakdown(Network_to_run,Node_memory):
    Total_distribution_nodes=0
    Number_of_layers=28
    for i in range(1,Number_of_layers+1):
        Network_path=get_path_layerwise(Network_to_run,i)
        buf=open(Network_path,'rb').read()
        model = tflite.Model.GetRootAsModel(buf, 0)
        model_base_size=sys.getsizeof(buf)

        if(i==1): # first layer
            z=Min_nodes_layer(model,model_base_size,1,0,Node_memory,1)
            print("Min nodes for layer: "+str(i)+": "+str(z))
            Total_distribution_nodes+= z
        elif(i<27): # arena dominated layer and not layer 27 or 28
            z=(Min_nodes_layer(model,model_base_size,0,0,Node_memory,1))
            print("Min nodes for layer: "+str(i)+": "+str(z))
            Total_distribution_nodes+= z
        elif(i==27): # later layer 27
            z=Min_nodes_layer(model,model_base_size,0,1,Node_memory,1)
            print("Min nodes for layer: "+str(i)+": "+str(z))
            Total_distribution_nodes+= z
        elif(i==28): # later layer 28
            z=Min_nodes_layer(model,model_base_size,0,2,Node_memory,1)
            print("Min nodes for layer: "+str(i)+": "+str(z))
            Total_distribution_nodes+= z
        #print(Total_distribution_nodes)
    print(Total_distribution_nodes)


def layer_chunk_exp(Network_to_run,Node_memory):
    Total_distribution_nodes=0
    i=1
    j=28
    for k in range(i+1,j+1):
        if(Minimize(i,k,Network_to_run,Node_memory)==0): # if the layer chunk is minimum
            if(k<j):
                print("Passed Layer chunk has lower minimum nodes for the current: i: "+str(i)+" and j: "+str(k))
            elif (k==(j)):
                print("Passed Terminal! Layer chunk has lower minimum nodes for the current: i: "+str(i)+" and j: "+str(k))
                Total_distribution_nodes+= Min_Layerchunks_i_j(i,k,Network_to_run,Node_memory,1)
                Network = get_path_chunk(Network_to_run,i,k)
                buf=open(Network,'rb').read()
                model_base = tflite.Model.GetRootAsModel(buf, 0)
                model_size=sys.getsizeof(buf)
                print("Model and arena size of baseline layer from layer: i: "+str(i)+" to j: "+str(k))
                print(model_size)
                print(calculate_arena_size(model_base))
                break
        else:
            if((k-1)==i): # if the two consecutive  layers (k-1) and (K) themselves individually need lesser layerwise nodes than chunk i-i+1. Then keep them as layerwise
                Total_distribution_nodes+= Min_Layerwise_i_j(i,k-1,Network_to_run,Node_memory,1)
            elif(k==(j)): # last layer
                Total_distribution_nodes+= Min_Layerwise_i_j(i,k,Network_to_run,Node_memory,1)
            # elif(k==(j) and (Minimize(i,k-1,Network_to_run,Node_memory)!=0) ):
            #     Total_distribution_nodes+= Min_Layerwise_i_j(i,k,Network_to_run,Node_memory,1)
                break
            else:
                print("Cross fuse block found from layer: i: "+str(i)+" to j: "+str(k-1))

                Network = get_path_chunk(Network_to_run,i,k-1)
                buf=open(Network,'rb').read()
                model_base = tflite.Model.GetRootAsModel(buf, 0)
                model_size=sys.getsizeof(buf)
                print("Model and arena size of baseline layer from layer: i: "+str(i)+" to j: "+str(k-1))
                print(model_size)
                print(calculate_arena_size(model_base))
                Total_distribution_nodes+= Min_Layerchunks_i_j(i,k-1,Network_to_run,Node_memory,1)

           
            i=k # k where it failed will be the new
    
    print("Total distributed nodes are: "+str(Total_distribution_nodes))




if __name__ == "__main__":
    
    #print(HAR_L2(2097152)) # For HAR we only perform intra-layer partition of its layer2 i.e. the second fully connected layer.
    Node_memory=(212991) #212991, 192 KB 196608, 512 KB: 524288 KB, 1024 KB: 1048576, 2048 KB: 2097152
    Network_to_run=1 # MNV1 is 1, MNV2 is 2, MNV3 is 3, MNV4 is 4, DSCNN is 5

    # Todo: Unomment the below two line to get the DENNI Intra-layer Min nodes for the benchmark used Network_to_run 
    # print("Running DENNI Intra-layer MINLP for Network: "+str(Network_to_run)+" with node memory constrain: "+str(Node_memory)+" KB")
    # print_layerwise_breakdown(Network_to_run,Node_memory)

    # Todo: Unomment the below two line to get the DENNI Cross-Fuse Min nodes for the benchmark used Network_to_run 
    # print("Running DENNI Intra-layer MINLP for Network: "+str(Network_to_run)+" with node memory constrain: "+str(Node_memory)+" KB")
    # layer_chunk_exp(Network_to_run,Node_memory)

   

   


    