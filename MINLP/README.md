# Mixed Integer Non linear programming (MINLP) formulation of Neural Network inference partitioning while minimizing the overall number of nodes
Goal of the script is to use MINLP to distributed various benchmarks (tflite integer 8 bit quantized convolutional neural networks) across minimum number of edge nodes with a provided memory constrain by greedly cross-fusing maximum layers to reduce overall tensor-arena size

# Steps to run
- First extract the folder MINLP_data.zip (located in the denni_ipccc\MINLP) which contains layerwise and layerchunk tensorflow-lite models for various benchmarks as the MINLP_script.py makes use of it. 
- MINLP uses the Gekko package (https://pypi.org/project/gekko/) to enable MINLP optimization. Install it using pip (pip install gekko)
- Also make sure you have both tflite and flatbuffers pip packages installed.
- Carefully update the absolute paths of the respective files as per where you uncompressed the folder MINLP_data.zip in the MINLP_script.py (line 930-960).
- Choose the network you want to execute MINLP by following instructions from line 1041 to 1051 in the MINLP_script.py
- You should be able to replicate the results for each benchmarks. The sample output will look like DENNI_sample_MINLP_Output.txt  




