# Mobilenetv4 layerwise inference memory footprint breakdown 
Goal of the script is to plot the total inference memory footprint breakdown for each layer of mobilenetv4 on msp430fr5994 when using a custom 16-bit tensorflow-lite implementation

# Steps to Run
- To replicate the mobilenetv4 layeewise results execute the script Mobilenet_layerwise_memory_breakdown.ipynb
