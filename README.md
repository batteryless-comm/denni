# DENNI_IPCCC

This project implements Distributed Neural Network Inference on Severely
Resource Constrained Edge Devices (DENNI). The article was published at 2021 IEEE 40th International Performance Computing and Communications Conference (IPCCC)
and can be found at this [link](https://ieeexplore.ieee.org/document/9679448) . This project use MSP430FR5994 as the main processor with CC2650R2F as radio board for BLE communication cost calculation.

The paper can be cited using the [ieee link](https://ieeexplore.ieee.org/document/9679448) or using following -

R. Sahu, R. Toepfer, M. D. Sinclair and H. Duwe, "DENNI: Distributed Neural Network Inference on Severely Resource Constrained Edge Devices," 2021 IEEE International Performance, Computing, and Communications Conference (IPCCC), 2021, pp. 1-10, doi: 10.1109/IPCCC51483.2021.9679448.

@INPROCEEDINGS{9679448,  author={Sahu, Rohit and Toepfer, Ryan and Sinclair, Mathew D. and Duwe, Henry},  booktitle={2021 IEEE International Performance, Computing, and Communications Conference (IPCCC)},   title={DENNI: Distributed Neural Network Inference on Severely Resource Constrained Edge Devices},   year={2021},  volume={},  number={},  pages={1-10},  doi={10.1109/IPCCC51483.2021.9679448}}

# System setup for msp430 for running our own custom 16-bit tensorflow-lite inference backend
- Install CCS (Code composer studio) IDE version - 9.2.0.00013
- Install the gcc compiler GNU v8.3.0.16 (Mitto Systems Limited) by using the [link](https://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/8_3_0_0/index_FDS.html)

# System setup for PC(x86) for running our own custom 16-bit tensorflow-lite inference backend
- Install the clion IDE version - 2020.3.4(tested) or above by using the [link](https://www.jetbrains.com/clion/)

# Python environment/dependencies for running the MINLP, Tensor-arena estimation and other scripts
- Create a python environment (could be any virtual env or anaconda env) 
- Install pip packages: gekko (1.0.5), flatbuffers (22.12.6), numpy (1.24.1), tflite (2.10.0)
- gekko [link](https://pypi.org/project/gekko/)
- flatbuffers [link](https://pypi.org/project/flatbuffers/)
- tflite [link](https://pypi.org/project/tflite/)

# To run the experiment
- Go to MINLP folder and execute the MINLP_script.py for the desired benchmark to get the min node and the partitioning and dataflow information of how to distribute the network.
- Then use the DENNI_tflite_partitioner to partition the baseline .tflite benchmark as per the partitioning info.
- Then use the CCS_msp430_code to execute these partitions on the msp430fr5994 and benchmark the latency and enrgy of computation.
- For correctness check or debugging use the Clion_pc_code to execute the model partitions. Compare them with the output of CCS_msp430_code
- Use the benchmarked communication latency & energy per byte between two cc2640R2F device as mentioned in the paper and simulate the distributed inference in python with both computation and   	 communication cost for a given benchmark to get the overall latency and energy results.
- For HAR pruning and Quantizaion refer the scripts in the folder HAR_Pruning_Quantization
- For Mobilenet layerwise and parto plots refer the scripts in the folder Mobilenet_Layerwise_Memory_Breakdown and Mobilenet_Pareto_plot
- For arena estimation refer the script located inside tensorflow_lite_arena_estimation.
- Each folders and script has respective to do's mentioned to be used while executing them.
